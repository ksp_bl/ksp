import traceback

from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.views.generic import FormView, ListView

from .forms import ImportForm
from .models import FileImport
from .imports import ImportErrors


class ImportedListView(ListView):
    queryset = FileImport.objects.order_by('-when')
    template_name = 'imports/list.html'


class ImportView(FormView):
    form_class = ImportForm
    template_name = 'imports/import.html'
    success_url = reverse_lazy('import_list')

    def form_valid(self, form):
        try:
            imp_file = form.save()
        except ImportErrors as err:
            messages.error(self.request, "Sorry, errors happened during file import:\n%s" % "\n".join(err.errors))
        except Exception as err:
            tb = traceback.format_exc()
            messages.error(self.request, "Unable to import data for file %s.\nError: %s\nTraceback: %s" % (
                form.cleaned_data['ifile'], err, tb))
        else:
            messages.success(self.request,
                "Data for file %s have been successfully imported." % (
                        form.cleaned_data['ifile'].name,))
            if imp_file.warnings:
                messages.warning(self.request, "Some warnings have been produced during import:\n%s" % imp_file.warnings)
        return super(ImportView, self).form_valid(form)

