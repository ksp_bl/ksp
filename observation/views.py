from collections import Counter, namedtuple, OrderedDict
import csv
import itertools
import json
import urllib

from django.contrib.gis.db.models import Union
from django.contrib.gis.geos import GEOSGeometry
from django.core.urlresolvers import reverse
from django.db import connection
from django.db.models import Avg, Case, CharField, Count, F, Model, StdDev, Sum, Value, When
from django.db.models.fields import FieldDoesNotExist
from django.db.models.functions import Coalesce
from django.forms import ChoiceField
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.utils.encoding import force_text
from django.views.generic import DetailView, FormView, View, TemplateView

from gemeinde.models import Gemeinde
from document.models import Document

from .forms import TreeEditForm, TreeReconcileForm
from .models import Phytosoc, Plot, PlotObs, TreeObs
# database views:
from .models import (
    HomepageView, AdminRegion, BWARTEN, BWNATURN, BWSTRU1M, Biotopwert, Einwuchs, EinwuchsProSpec,
    HolzProduktion, HolzProduktionProSpec, Nutzung, NutzungProSpec, Totholz, TotholzProSpec,
    Zuwachs,
)

ViewTuple = namedtuple('ViewTuple', ['name', 'oid'])

inv_choice = ChoiceField(choices=(('', "Alle Aufnahmen"), ('1', "Erste Aufnahme"), ('2', "Zweite Aufnahme")))


class GemeindenView(TemplateView):
    """
    Calculate the number of PlotObs per municipality and year.
    """
    template_name = 'gemeinden.html'

    def get_context_data(self, **context):
        context = super(GemeindenView, self).get_context_data(**context)
        # {'Aesch': {'obj': <Aesch Gemeinde>}, ... }
        gemeinden = OrderedDict([(gem.name, {'obj': gem})
                                 for gem in Gemeinde.objects.all().order_by(
                                    Case(When(kanton='BL', then=Value('')),
                                         When(kanton='BS', then=Value('')), default=F('kanton'),
                                         output_field=CharField()),
                                    'name')])
        figures = HomepageView.objects.all()
        inv_period = self.request.GET.get('aufn', None)
        if inv_period:
            figures = figures.filter(inv_period=int(inv_period))
        for fig in figures:
            gemeinden[fig.gemeinde].setdefault('years', []).append(fig)
        for key, value in gemeinden.items():
            # Add kanton to name if it's not BS or BL
            value['name'] = key if value['obj'].kanton in ['BS', 'BL'] else '%s (%s)' % (key, value['obj'].kanton)
        context.update({
            'gemeinden': gemeinden,
            'db_view': view_tuple(HomepageView._meta.db_table),
            'inv_choice': inv_choice.widget.render('inventory_filter', inv_period),
        })
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('format') == 'csv':
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="basel-gemeinden.csv"'
            writer = csv.writer(response, delimiter=";")
            writer.writerow([s.encode('utf-8') for s in [
                'Gemeinde', 'Jahr', 'Anzahl Probepunkte', 'Anzahl Bäume', 'theoretische Waldfläche pro ha',
                'Stammzahl pro ha', 'Volumen [m3 pro ha]', 'Grundflaeche [m2 pro ha]',
            ]])
            for name, infos in context['gemeinden'].items():
                if 'years' not in infos:
                    writer.writerow([data.gemeinde.encode('utf-8')] + ['-'] * 7)
                    continue
                for data in infos['years']:
                    writer.writerow([
                        data.gemeinde.encode('utf-8'), data.jahr, data.probepunkte, data.probebaum_abs,
                        data.waldflaeche, data.stammzahl_ha, data.volumen_ha, data.grundflaeche_ha,
                    ])
            return response
        return super(GemeindenView, self).render_to_response(context, **response_kwargs)


class GemeindeDetailView(TemplateView):
    template_name = 'gemeinde_detail.html'

    def get_context_data(self, **context):
        context = super(GemeindeDetailView, self).get_context_data(**context)
        gemeinde = get_object_or_404(Gemeinde, pk=self.kwargs['pk'])
        context.update({
            'gemeinde': gemeinde,
            'center': gemeinde.the_geom.centroid,
            'plot_obs': gemeinde.plotobs_set.values('year').annotate(Count('id')).order_by('year'),
        })
        return context


class PlotDetailView(DetailView):
    model = Plot
    template_name = 'plot_detail.html'

    def post(self, request, *args, **kwargs):
        """
        Allow manually marking a plot as checked. Could be removed once cleaning plots is finished.
        """
        if request.POST.get('checked') == 'true':
            plot = get_object_or_404(Plot, pk=kwargs['pk'])
            plot.checked = True
            plot.save()
            return HttpResponseRedirect(request.path)

    def get_context_data(self, **context):
        context = super(PlotDetailView, self).get_context_data(**context)
        gem = Gemeinde.objects.filter(the_geom__contains=self.object.the_geom).first()
        if gem:
            neighbours = Gemeinde.objects.filter(the_geom__bboverlaps=gem.the_geom).exclude(pk=gem.pk)
        else:
            # Not in a recognized municipality, find the nearest one(s)
            distance = self.object.the_geom.distance(self.object.plotobs_set.first().municipality.the_geom) + 1
            neighbours = Gemeinde.objects.filter(the_geom__dwithin=(self.object.the_geom, distance))
        observations = self.object.plotobs_set.prefetch_related('treeobs_set').order_by('year')

        # Compare trees between observations and mark trees found in all plotobs as `found`
        counter = Counter()
        trees = [obs.treeobs_set.select_related('tree', 'tree__spec', 'vita').all().order_by('tree__nr')
                 for obs in observations]
        for treelist in trees:
            counter.update([tr.tree_id for tr in treelist])
        for treelist in trees:
            for tobs in treelist:
                if counter[tobs.tree_id] == len(trees):
                    tobs.css = 'sync'
                elif counter[tobs.tree_id] > 1:
                    tobs.css = 'sync_partial'
                else:
                    tobs.css = 'nosync'
        observations = zip(observations, trees)
        context.update({
            'observations': observations,
            'real_municip': gem,
            'neighbours': neighbours,
        })
        return context


class PlotObsDataView(View):
    def get(self, request, *args, **kwargs):
        plotobs = PlotObs.objects.select_related('plot').filter(year=request.GET.get('year'), municipality_id=request.GET.get('gem'))
        struct = {"type": "FeatureCollection", "features": []}
        for obs in plotobs:
            struct['features'].append({
                "type": "Feature",
                "geometry": eval(obs.plot.the_geom.json),
                "properties": { "pk": obs.pk, "url": reverse('plotobs_detail', args=[obs.pk]) },
            })
        return HttpResponse(json.dumps(struct), content_type='application/json')


class PlotObsDetailView(TemplateView):
    output = 'json'

    def get_template_names(self):
        return 'plotobs_detail_core.html' if self.request.is_ajax() else 'plotobs_detail.html'

    def get_context_data(self, **context):
        self.plotobs = get_object_or_404(PlotObs.objects.select_related('plot'), pk=self.kwargs['pk'])
        return {
            'geojson': self.plotobs.as_geojson(dumped=False),
            'center': self.plotobs.plot.the_geom,
            'plotobs': self.plotobs,
            'hidden_props': ('id', 'area', 'subsector', 'evaluation_unit', 'abt',
                             'type', 'Aufnahmepunkt (plot)', 'Aufnahmejahr'),
        }

    def render_to_response(self, context, **kwargs):
        if self.output == 'json':
            return JsonResponse(context['geojson'])
        else:
            context.update({
                'properties': context['geojson']['features'][0]["properties"],
                'geojson': json.dumps(context['geojson']),
                'gemeinde': self.plotobs.municipality,
                'treeobs': self.plotobs.treeobs_set.select_related('tree').all().order_by('tree__nr'),
                'siblings': self.plotobs.plot.plotobs_set.exclude(pk=self.plotobs.pk),
            })
            return super(PlotObsDetailView, self).render_to_response(context, **kwargs)

def plots_to_check(request):
    context = {
        'plots': Plot.objects.filter(checked=False).order_by('nr'),
    }
    return render(request, 'plots_to_check.html', context)

class TreeReconcileView(FormView):
    form_class = TreeReconcileForm
    template_name = 'tree_reconcile.html'

    def get_form_kwargs(self):
        kwargs = super(TreeReconcileView, self).get_form_kwargs()
        kwargs.update({
            'tree1': get_object_or_404(TreeObs, pk=self.request.GET.get('tree1')),
            'tree2': get_object_or_404(TreeObs, pk=self.request.GET.get('tree2')),
        })
        return kwargs

    def form_valid(self, form):
        form.save()
        return HttpResponse('OK')


class TreeEditView(FormView):
    form_class = TreeEditForm
    template_name = 'tree_edit.html'

    def get_form_kwargs(self):
        kwargs = super(TreeEditView, self).get_form_kwargs()
        kwargs.update({'tree_obs': get_object_or_404(TreeObs, pk=self.kwargs['pk'])})
        return kwargs

    def form_valid(self, form):
        form.save()
        return HttpResponse('OK')


"""{'View key' (from data form):
    'model': View class,
    'model_by_spec': alternate model to use when grouping by species,
    'plot_obs': 'prefix to plot_obs.id',
    'order_by': 'field on which to order',
    'map_color_fields':
    'annot_map' (optional): when an aggregation occurs, fields and corresponding annotation to use,
}"""
VIEW_MAP = {
    'BWNATURN': {
        'model': BWNATURN, 'plot_obs': 'plot_obs__', 'order_by': 'plot_obs',
        'map_color_fields': ['bwnaturn', 'fichte_perc', 'nadel_perc', 'nadel_wtanne_perc'],
        'annot_map': {'bwnaturn': Avg},
        'groupables': ['bwnaturn', 'code'],
    },
    'BWARTEN': {
        'model': BWARTEN, 'plot_obs': 'plot_obs__', 'plot': 'plot_obs__plot', 'order_by': 'plot_obs',
        'map_color_fields': ['bwarten', 'num_spec', 'special_spec'],
        'annot_map': {'bwarten': Avg, 'num_spec': Avg, 'special_spec': Avg},
        'groupables': ['bwarten'],
    },
    'BWSTRU': {
        'model': BWSTRU1M, 'plot_obs': 'id__', 'plot': 'id__plot', 'order_by': 'id',
        'map_color_fields': ['bwstru1m'],
        'annot_map': {'bwstru1m': Avg, 'stand_devel_stage': Avg,
                      'stand_crown_closure': Avg, 'stand_structure': Avg},
        'groupables': ['bwstru1m'],
    },
    'Biotopwert': {
        'model': Biotopwert, 'plot_obs': 'id__', 'order_by': 'id',
        'map_color_fields': ['biolfi1m', 'bioklass'],
        'annot_map': {'biolfi1m': Avg},
        'groupables': ['owner_type', 'bioklass'],
    },
    'Stammzahl': {
        'model': HolzProduktion, 'model_by_spec': HolzProduktionProSpec,
        'plot_obs': 'plotobs_id__', 'order_by': 'plotobs',
        'map_color_fields': ['anzahl_baume_rel'],
        'annot_map': {'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        'groupables': ['spec', 'owner_type', 'stand_devel_stage'],
    },
    'Totholz': {
        'model': Totholz, 'model_by_spec': TotholzProSpec,
        'plot_obs': 'plotobs_id__', 'order_by': 'plotobs',
        'map_color_fields': ['anzahl_baume_rel'],
        'annot_map': {'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        'groupables': ['spec'],
    },
    'Einwuchs': {
        'model': Einwuchs, 'model_by_spec': EinwuchsProSpec,
        'plot_obs': 'plotobs_id__', 'order_by': 'plotobs',
        'map_color_fields': ['anzahl_baume_rel'],
        'annot_map': {'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        'groupables': ['spec'],
    },
    'Nutzung': {
        'model': Nutzung, 'model_by_spec': NutzungProSpec,
        'plot_obs': 'plotobs_id__', 'order_by': 'plotobs',
        'map_color_fields': ['anzahl_baume_rel'],
        'annot_map': {'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        'groupables': ['spec'],
    },
    'Zuwachs': {
        'model': Zuwachs,
        'plot_obs': 'id__', 'order_by': 'id',
        'map_color_fields': [],
        'annot_map': {'growth_ha': Avg},
        'groupables': [],
    },
    # Essentially to query verbose field names for some groupable keys not on views
    'plotobs': {'model': PlotObs},
}

class PermissiveEncoder(json.JSONEncoder):
    """
    Custom encoder to simply return 'null' for unserializable objects.
    """
    def default(self, obj):
        if issubclass(obj, Model):
            return dict([(f.name, f.verbose_name) for f in obj._meta.fields])
        try:
            return super(PermissiveEncoder, self).default(obj)
        except TypeError:
            return None


class DataPageView(TemplateView):
    template_name = 'data_page.html'

    def get_context_data(self, **context):
        context = super(DataPageView, self).get_context_data(**context)
        context['perimeters'] = [
            {
                'items': Gemeinde.objects.all().order_by('name'),
                'title': "Gemeinden",
                'infos': """Politische Gemeinden der beiden Basel sowie einzelne Gemeinden aus den
                         Nachbarkantonen, in den Kontrollstichproben durch das Amt für Wald
                         beider Basel erhoben wurden.""",
                'short_name': "gem",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='Forstkreis').order_by('name'),
                'title': "Forstkreise",
                'infos': "Alle Forstkreise der beiden Basel können ausgewählt werden.",
                'short_name': "fkreis",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='Forstrevier').order_by('name'),
                'title': "Forstreviere",
                'infos': "Alle Forstreviere der beiden Basel können ausgewählt werden.",
                'short_name': "frevier",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='Jagdrevier').order_by('name'),
                'title': "Jagdreviere",
                'infos': "Alle Jagdreviere der beiden Basel können ausgewählt werden.",
                'short_name': "jrevier",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='WEP').order_by('name'),
                'title': "WEP",
                'infos': "Alle WEPs der beiden Basel können ausgewählt werden.",
                'short_name': "wep",
            },
             {
                'items': AdminRegion.objects.filter(region_type__name='Eigentümer').order_by('name'),
                'title': "Eigentümer",
                'infos': "Alle Waldeigentümer der beiden Basel können ausgewählt werden.",
                'short_name': "eigentumer",
            },
        ]
        context.update({
            'views': json.dumps(VIEW_MAP, cls=PermissiveEncoder),
        })
        return context

Field = namedtuple('Field', ['name', 'accessor', 'fkey_target', 'vname'])

class DataGridView(TemplateView):
    template_name = 'data_grid.html'
    # Mapping to get the 'interesting' field on a foreign key join
    fkey_map = {
        'stand_devel_stage': 'description',
        'stand_crown_closure': 'description',
        'stand_structure': 'description',
        'municipality': 'name',
        'gemeinde': 'name',
        'plot': 'nr',
        'owner_type': 'description',
        'spec': 'species',
    }

    def get(self, request, *args, **kwargs):
        bio_analyse = self.request.GET.get('bio')
        if not bio_analyse:
            return self.render_to_response({'error': "Keine ausgewählte Analyse"})
        try:
            self.model_data = VIEW_MAP[bio_analyse]
        except KeyError:
            return self.render_to_response({'error': "Unerkannte Analysename"})

        self.get_perimeter()
        self.fields = [Field(f.name, f.name, self.get_fkey_target(f.name), f.verbose_name)
                  for f in self.model_data['model']._meta.fields]
        if self.request.GET.get('format') == 'json':
            self.fields.append(Field('the_geom', '%splot__the_geom' % self.model_data['plot_obs'], '', 'Geometry'))

        if request.GET.get('metadata'):
            # Only return query metadata
            map_color_fields = [(f.name, f.vname) for f in self.fields if f.name in self.model_data['map_color_fields']]
            gem_names = Gemeinde.objects.filter(pk__in=request.GET.getlist('gem')).values_list('name', flat=True).order_by('name')
            return JsonResponse({
                'gemeinden': list(gem_names),
                'map_color_fields': map_color_fields,
            }, safe=False)

        context = self.get_context_data(**kwargs)
        if request.GET.get('format') == 'json' and not 'error' in context:
            # JSON data for the map view
            return JsonResponse(as_geojson(context['field_names'], context['query'], context['gemeinden']))
        else:
            return self.render_to_response(context)

    def get_perimeter(self):
        """Get perimeter choices from request"""
        def params_to_int(key):
            return [int(v) for v in self.request.GET.getlist(key)]

        self.perimeter = {
            'gems': params_to_int('gem'),
            'regions': (params_to_int('fkreis') + params_to_int('frevier') +
                        params_to_int('jrevier') + params_to_int('wep') +
                        params_to_int('eigentumer'))
        }

    def get_fkey_target(self, fname):
        if fname in self.fkey_map:
            return '__%s' % self.fkey_map[fname]
        return ''

    def build_query(self, aggrs, stddev=False):
        coalesce_with_0 = False
        view_model = self.model_data['model']
        plot_prefix = self.model_data.get('plot', 'plot')
        plot_obs_prefix = self.model_data['plot_obs']

        if aggrs:
            if 'spec' in aggrs and 'model_by_spec' in self.model_data:
                view_model = self.model_data['model_by_spec']
                specf = view_model._meta.get_field('spec')
                self.fields.append(Field(specf.name, specf.name, self.get_fkey_target(specf.name), specf.verbose_name))
                coalesce_with_0 = True
            # Adding Phytosoc field metadata when used in groupments
            if 'phyto_code' in aggrs:
                self.fields.append(
                    Field('phyto_code', '%s__phytosoc__code' % plot_prefix, '', 'Phytosoziologie')
                )
            if 'inc_class' in aggrs:
                self.fields.append(
                    Field('inc_class', '%s__phytosoc__inc_class' % plot_prefix, '', Phytosoc._meta.get_field('inc_class').verbose_name)
                )
            if 'ecol_grp' in aggrs:
                self.fields.append(
                    Field('ecol_grp', '%s__phytosoc__ecol_grp' % plot_prefix, '', Phytosoc._meta.get_field('ecol_grp').verbose_name)
                )

        # Limit the query with choices from the "Perimeter" section
        if self.perimeter['gems']:
            query = view_model.objects.filter(**{'%smunicipality__pk__in' % plot_obs_prefix: self.perimeter['gems']})
        elif self.perimeter['regions']:
            query = view_model.objects.filter(
                **{'%s__the_geom__within' % plot_prefix:
                   AdminRegion.objects.filter(pk__in=self.perimeter['regions']).aggregate(geom=Union('geom'))['geom']}
            )
        else:
            raise ValueError("No selected perimeter")
        field_names = [f.vname for f in self.fields]  # Extract verbose names

        if aggrs:
            if 'year' not in aggrs and 'inv_period' not in aggrs:
                # Always add year, as grouping and mixing years makes no sense
                aggrs.append('year')
            # Only get grouped-by fields + fields with an available aggregation
            all_aggr_fields = {}
            if 'annot_map' in self.model_data:
                all_aggr_fields.update(self.model_data['annot_map'])
            fdict = dict([(f.name, f) for f in self.fields])
            field_names = []

            aggr_crits = []
            for fname in aggrs:
                # Privilege field join on plot_obs
                try:
                    field = PlotObs._meta.get_field(fname)
                    if field.auto_created and not field.concrete:
                        raise FieldDoesNotExist
                    aggr_crits.append('%s%s%s' % (plot_obs_prefix, fname, self.get_fkey_target(fname)))
                    field_names.append('*%s' % getattr(field, 'verbose_name', field.name))
                except FieldDoesNotExist:
                    if fname in fdict:
                        # Hopefully the field is in fdict
                        aggr_crits.append(fdict[fname].accessor + fdict[fname].fkey_target)
                        field_names.append('*%s' % fdict[fname].vname)
                    else:
                        if fname in {'forstkreis', 'forstrevier', 'jagdrevier', 'WEP', 'eigentümer'}:
                            aggr_crits.append('%s__plotinregionview__region_name' % plot_prefix)
                            fname_cap = fname[0].upper() + fname[1:]
                            field_names.append('*%s' % fname_cap)
                            query = query.filter(
                                **{'%s__plotinregionview__region_type' % plot_prefix: fname_cap}
                            )

            annot_list = []
            for f in self.fields:
                # Last condition is a special case: when aggregating by bioklass,
                # no need to display the biolfi1m value.
                if (not f.name in all_aggr_fields or f.name in aggrs
                        or (f.name == 'biolfi1m' and 'bioklass' in aggrs)):
                    continue
                annot_alias = '%s__%s' % (f.name, all_aggr_fields[f.name].name.lower())
                if coalesce_with_0:
                    annot_list.append(all_aggr_fields[f.name](Coalesce(f.accessor, Value(0))))
                    # mandatory default_alias
                    annot_list[-1].source_expressions[0].name = annot_alias
                else:
                    annot_list.append(all_aggr_fields[f.name](f.accessor))
                field_names.append(fdict[f.name].vname)
                if stddev:
                    annot_list.append(StdDev(f.accessor))
                    field_names.append("%s (Standardabweichung)" % fdict[f.name].vname)

            annot_list.append(Count(self.fields[0].name, distinct=True))  # count on id/obs_id
            field_names.append('Anzahl KSP')
            query = query.values_list(*aggr_crits).annotate(*annot_list).order_by(*aggr_crits)

        else:
            field_list = [f.accessor + f.fkey_target for f in self.fields]
            order_by = self.model_data['order_by']
            query = query.values_list(*field_list).order_by(order_by)
        # debug: print(str(query.query))
        return query, field_names

    def get_context_data(self, **context):
        context = super(DataGridView, self).get_context_data(**context)
        # Compute data after request.GET
        aggrs = self.request.GET.getlist('yaggr') + self.request.GET.getlist('paggr') \
                + self.request.GET.getlist('aggr')
        stddev = bool(self.request.GET.get('stddev'))

        context['gemeinden'] = Gemeinde.objects.filter(pk__in=self.perimeter['gems']).order_by('name')
        context['regions'] = AdminRegion.objects.filter(pk__in=self.perimeter['regions']).order_by('name')
        try:
            context['query'], context['field_names'] = self.build_query(aggrs, stddev)
            if not aggrs:
                context['total'] = context['query'].count()
        except Exception as exc:
            context['error'] = "Leider ist ein Fehler aufgetreten. (%s)" % force_text(exc)
        else:
            context['db_view'] = view_tuple(context['query'].model._meta.db_table)
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('format') == 'csv' and 'error' not in context:
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="ksp-data-export.csv"'
            writer = csv.writer(response, delimiter=";")
            writer.writerow(context['field_names'])
            for line in context['query']:
                writer.writerow([to_csv(val) for val in line])
            return response
        return super(DataGridView, self).render_to_response(context, **response_kwargs)


class ViewDefinition(TemplateView):
    template_name = "view_def.html"

    def get_context_data(self, **context):
        context = super(ViewDefinition, self).get_context_data(**context)
        cursor = connection.cursor()
        cursor.execute("select pg_get_viewdef(%s, true);", [int(self.kwargs['oid'])]);
        try:
            context['definition'] = cursor.fetchone()[0]
        except TypeError:
            raise Http404
        cursor.execute("select relname from pg_class where oid=%s;", [self.kwargs['oid']]);
        context['name'] = cursor.fetchone()[0]
        return context


class DocumentationView(TemplateView):
    template_name = "docs.html"

    def get_context_data(self, **context):
        context = super(DocumentationView, self).get_context_data(**context)
        context['docs'] = Document.objects.all().order_by('-weight')
        return context


VIEWS = {
    'anzahl_pro_gemein': {'name': "Anzahl Stichproben pro Gemeinde und Aufnahmejahr"},
    #'stammzahl_pro_gemein':
    'einwuchs_pro_gemein': {
        'name': "Einwuch pro Gemeinde (sv/ha)",
        'graph': ((1, "Einwuchs"), (2, "% Standardfehler"))},
    'biolfi1m_pro_gemein': {
        'name': "BIOLFI1M_pro Gemeinde",
        'graph': ((1, "Biotowert"), (3, "Wert der Schichtung"))},
    'biolfi1m_braendli_pro_gemein': {
        'name': "BIOLFI1M_pro Gemeinde_Klassen nach LFI3_lit_braendli",
        'graph': ((1, "high"), (2, "tends to be high"), (3, "tends to be low"), (4, "low"))},
}

def chart(request, view_name):
    try:
        from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
        from matplotlib.figure import Figure
    except ImportError:
        return HttpResponse()

    view_data = select_from_view(request, view_name)
    cols = VIEWS[view_name]['graph']
    xlabels = [str(Gemeinde.from_code(d[0])) for d in view_data]

    fig = Figure(facecolor='white')
    ax = fig.add_subplot(1,1,1)
    # Draw data axes
    for (col_index, col_name) in VIEWS[view_name]['graph']:
        data = [d[int(col_index)] for d in view_data]
        ax.plot(range(0, len(data)), data, 'o-', label=col_name)
        #ax.bar(range(0, len(data)), data, 0.35, label=col_name)

    ax.set_xticks(range(0, len(xlabels)))
    ax.set_xticklabels(xlabels, rotation=30, horizontalalignment='right')
    ax.grid(True)
    leg = ax.legend(loc='best', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    fig.subplots_adjust(bottom=0.18, left=0.15)

    canvas = FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response


# Utilities

def select_from_view(request, view_name):
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM "%s"' % VIEWS[view_name]['name'], [])
    return filter_by_gemein(request, cursor.fetchall())

def view_tuple(relname):
    """Return (view_name, view_oid) tuple"""
    cursor = connection.cursor()
    cursor.execute("select oid from pg_class WHERE relname=%s", [relname]);
    try:
        oid = cursor.fetchone()[0]
    except TypeError:
        oid = None
    return ViewTuple(relname, oid)

def filter_by_gemein(request, result, gem_field=0):
    selected_gems = request.COOKIES.get('sel_gems')

    def filter_gem(res):
        if isinstance(gem_field, int):
            return res[gem_field] in selected_gems
        else:
            return res.get(gem_field) in selected_gems
    if selected_gems:
        selected_gems = urllib.unquote(selected_gems).split(',')
        return filter(filter_gem, result)
    return result

def as_geojson(field_names, query, gemeinden):
    """
    Output a queryset `query` as GeoJSON, including geometries for
    municipality `gemeinden`.
    """
    struct = {"type": "FeatureCollection", "features": []}
    for line in query:
        struct['features'].append({
            "type": "Feature",
            "geometry": eval(GEOSGeometry(line[-1]).json),
            "properties": dict(zip(field_names[:-1], line[:-1])),
        })
    for gem in gemeinden:
        struct['features'].append({
            "type": "Feature",
            "geometry": eval(gem.the_geom.json),
            "properties": {'name': gem.name},
        })
    return struct

def to_csv(value):
    if value is None:
        return ''
    return value
