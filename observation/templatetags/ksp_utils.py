# *-* encoding: utf-8 *-*
from __future__ import unicode_literals

from decimal import Decimal

from django import template
from django.contrib.gis.gdal import SpatialReference, CoordTransform
from django.utils import numberformat

from gemeinde.models import Gemeinde

register = template.Library()

ch1903 = SpatialReference(21781)
ch1903plus = SpatialReference(2056)
trans = CoordTransform(ch1903, ch1903plus)

@register.filter
def gem_name(gem_code):
    """
    Display a municipality plz/name from its code (A4355).
    """
    gem = Gemeinde.from_code(gem_code)
    if not gem:
        return gem_code
    return "%s %s" % (gem.plz, gem.name)

@register.filter
def gem_name_list(gem_qs):
    """
    Display a comma-separated list of municipality names from a queryset.
    """
    return ", ".join([gem.name for gem in gem_qs])

@register.filter(is_safe=False)
def display_value(value):
    """Handle display of values with None or decimals."""
    if value is None:
        return ''
    elif isinstance(value, (float, Decimal)):
        return numberformat.format(value, ',', decimal_pos=1, grouping=3, thousand_sep=' ')
    return value

@register.filter
def coords_to_js(geom):
    return eval(geom.json)['coordinates']

@register.filter
def as_geojson(geom):
    if geom.srid != 2056:
        geom.transform(trans)
    return ('{"type": "Feature", "geometry":%s,'
            ' "crs": { "type": "name", "properties": { "name": "urn:x-ogc:def:crs:EPSG:%s" } } }' % (geom.json, geom.srid))
