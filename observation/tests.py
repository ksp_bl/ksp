import json

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase


class ObservationTest(TestCase):
    fixtures = ('gemeinde.json',)

    def setUp(self):
        User.objects.create_user(username='user', password='password')

    def test_datagrid_metadata(self):
        self.client.login(username='user', password='password')
        response = self.client.get(reverse('data_grid') + "?gem=1&bio=BWARTEN&metadata=1&format=json")
        result = json.loads(response.content.decode('utf-8'))
        self.assertEqual(result, {
            'map_color_fields': [
                ['bwarten', 'Gehölzartenvielfalt (BWARTEN)'],
                ['num_spec', 'Anzahl Baumarten'],
                ['special_spec', 'Special Species'],
            ],
            'gemeinden': ['Arisdorf'],
        })
