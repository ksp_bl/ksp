"""
**************
Database Views
**************
"""
from django.db import models

from gemeinde.models import Gemeinde
from .models import (
    AdminRegion, CrownClosure, DevelStage, OwnerType, Plot, PlotObs,
    StandStructure, TreeSpecies,
)


class HomepageView(models.Model):
    gemeinde = models.CharField(max_length=50, db_column="gemeindename")
    jahr = models.SmallIntegerField("Aufnahmejahr", db_column="year")
    probepunkte = models.IntegerField("Anzahl Probepunkte", db_column="Anzahl Probepunkte")
    waldflaeche = models.DecimalField("theoretische Waldfläche pro ha", max_digits=5, decimal_places=1,
        db_column="theoretische Waldfläche ha")
    probebaum_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    stammzahl_ha = models.IntegerField("Stammzahl pro ha", db_column="Stammzahl pro ha")
    #stammzahl_stddev = models.FloatField("?", db_column="% Standardfehler")
    volumen_ha = models.DecimalField("Volumen [m3 pro ha]", max_digits=5, decimal_places=1,
        db_column="Volumen pro ha")
    #volumen_stddev = models.FloatField("?", db_column="% Standardfehler2")
    grundflaeche_ha = models.DecimalField("Grundflaeche [m2 pro ha]", max_digits=5, decimal_places=1,
        db_column="Grundflaeche pro ha")
    #grundflaeche_stddev = models.FloatField("?", db_column="% Standardfehler3")
    inv_period = models.SmallIntegerField()

    class Meta:
        db_table = 'web_homepage_holzproduktion'
        managed = False


class PlotInRegionView(models.Model):
    plot = models.ForeignKey(Plot)
    adminregion = models.ForeignKey(AdminRegion)
    region_name = models.CharField(max_length=100)
    region_type = models.CharField(max_length=20)

    class Meta:
        db_table = 'plot_in_region'
        managed = False


class BWARTEN(models.Model):
    plot_obs = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", on_delete=models.DO_NOTHING)
    bwarten = models.IntegerField(db_column='BWARTEN', verbose_name="Gehölzartenvielfalt (BWARTEN)")
    num_spec = models.IntegerField(db_column='Anzahl Baumarten', verbose_name="Anzahl Baumarten")
    special_spec = models.IntegerField(db_column='Special Species', verbose_name="Special Species")

    class Meta:
        db_table = 'bwarten_anz_baumarten_special_species'
        managed = False


class BWNATURN(models.Model):
    plot_obs = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    code = models.CharField(max_length=6)
    bwnaturn = models.IntegerField(db_column="BWNATURN", verbose_name="Naturnähe des Nadelholzanteils (BWNATURN)")
    fichte_perc = models.FloatField(db_column="%% Basalflächenanteil der Fichte",
        verbose_name="% Basalflächenanteil der Fichte")
    nadel_perc = models.FloatField(db_column="%% Basalflächenanteil Nadelholz",
        verbose_name="% Basalflächenanteil Nadelholz")
    nadel_wtanne_perc = models.FloatField(db_column="%% Basalflächenanteil Nadelholz ohne Tanne",
        verbose_name="% Basalflächenanteil Nadelholz ohne Tanne")

    class Meta:
        db_table = 'bwnaturn_ndh_fi_ndh_ohne_ta'
        managed = False


class BWSTRU1M(models.Model):
    id = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", db_column='id',
        on_delete=models.DO_NOTHING)
    stand_devel_stage = models.ForeignKey(DevelStage, verbose_name="Entwicklungsstufe",
        on_delete=models.DO_NOTHING)
    stand_crown_closure = models.ForeignKey(CrownClosure, verbose_name="Schlussgrad des Bestandes",
        on_delete=models.DO_NOTHING)
    stand_structure = models.ForeignKey(StandStructure, verbose_name="Schichtung des Bestandes",
        on_delete=models.DO_NOTHING)
    bwstru1m = models.IntegerField(db_column="BWSTRU1M", verbose_name="Strukturvielfalt (BWSTRU1M)")

    class Meta:
        db_table = 'bwstru1m'
        managed = False


class Biotopwert(models.Model):
    id = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", db_column='id',
        on_delete=models.DO_NOTHING)
    municipality = models.ForeignKey(Gemeinde, verbose_name="Gemeinde", on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")
    owner_type = models.ForeignKey(OwnerType, verbose_name="Besitzertyp", null=True, blank=True,
        on_delete=models.DO_NOTHING)
    biolfi1m = models.FloatField(verbose_name="Biotopwert (BIOLFI1M)", db_column="BIOLFI1M")
    bioklass = models.SmallIntegerField(verbose_name="Biotopwert Klasse", db_column="Biotopwert Klassen")

    class Meta:
        db_table = "Biotopwert Klassen pro plot_obs, year, owner_type_id"
        managed = False


class BaseHolzProduktion(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    plotobs = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
    anzahl_baume_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    anzahl_baume_rel = models.FloatField("Stammzahl/ha", db_column="Stammzahl pro ha")
    volumen_abs = models.DecimalField("Volumen [m3]", max_digits=5, decimal_places=1, db_column="Volumen m3")
    volumen_rel = models.FloatField("Volumen [m3/ha]", db_column="Volumen pro ha")
    grundflaeche_abs = models.FloatField("Grundfläche [m2]", db_column="Grundflaeche m2")
    grundflaeche_rel = models.FloatField("Grundfläche [m2/ha]", db_column="Grundflaeche pro ha")

    class Meta:
        abstract = True


class HolzProduktion(BaseHolzProduktion):
    class Meta:
        db_table = "basis_grundf_vol_yx_pro_plotobs"
        managed = False


class ModelProSpec(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    id = models.CharField(max_length=20, primary_key=True)
    plotobs = models.ForeignKey(PlotObs, db_column='plot_obs_id', on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
    spec = models.ForeignKey(TreeSpecies, verbose_name="Baumart", db_column='tree_spec_id', on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
    anzahl_baume_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    anzahl_baume_rel = models.FloatField("Stammzahl/ha", db_column="Stammzahl pro ha")
    volumen_abs = models.DecimalField("Volumen [m3]", max_digits=5, decimal_places=1, db_column="Volumen m3")
    volumen_rel = models.FloatField("Volumen [m3/ha]", db_column="Volumen pro ha")
    grundflaeche_abs = models.FloatField("Grundfläche [m2]", db_column="Grundflaeche m2")
    grundflaeche_rel = models.FloatField("Grundfläche [m2/ha]", db_column="Grundflaeche pro ha")

    class Meta:
        abstract = True

class HolzProduktionProSpec(ModelProSpec):

    class Meta:
        db_table = "basis_grundf_vol_yx_pro_plotobs_und_allspec"
        managed = False


class Totholz(BaseHolzProduktion):
    class Meta:
        db_table = "basis_grundf_vol_yx_pro_plotobs_totholz"
        managed = False


class TotholzProSpec(ModelProSpec):
    class Meta:
        db_table = "basis_grundf_vol_yx_pro_plotobs_und_allspec_totholz"
        managed = False


class Einwuchs(BaseHolzProduktion):
    class Meta:
        db_table = "basis_grundf_vol_yx_pro_plotobs_einwuchs"
        managed = False


class EinwuchsProSpec(ModelProSpec):
    class Meta:
        db_table = "basis_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs"
        managed = False


class Nutzung(BaseHolzProduktion):
    class Meta:
        db_table = "basis_grundf_vol_yx_pro_plotobs_nutzung"
        managed = False


class NutzungProSpec(ModelProSpec):
    class Meta:
        db_table = "basis_grundf_vol_yx_pro_plotobs_und_allspec_nutzung"
        managed = False


class Zuwachs(models.Model):
    id = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    growth = models.FloatField("Zuwachs/Jahr")
    growth_ha = models.FloatField("Zuwachs/ha/Jahr")

    class Meta:
        db_table = "basis_zuwachs_pro_plotobs"
        managed = False
