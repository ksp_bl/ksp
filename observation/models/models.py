import json
import math
from collections import OrderedDict

from django.contrib.gis.db import models as gis_models
from django.contrib.gis.geos import Point
from django.core.urlresolvers import reverse
from django.db import models, ProgrammingError
from django.db.utils import ConnectionDoesNotExist

from gemeinde.models import Gemeinde

OBSERVATION_FIXTURES = (
    'acidity.json', 'crown_form.json', 'crown_length.json', 'crown_closure.json',
    'damage.json', 'damage_cause.json', 'forest_form.json', 'forest_mixture.json',
    'gap.json', 'devel_stage.json', 'geology.json', 'layer.json', 'owner_type.json',
    'rank.json', 'regen_type.json', 'region.json', 'relief.json', 'sector.json',
    'soil_compaction.json', 'stand_structure.json', 'stem.json', 'survey_type.json',
    'vita.json', 'tree_species.json',
)

# This table maps the slope to the radius of the plot.
SLOPE_MAPPING = {
    20: 9.77,
    25: 9.92,
    30: 9.98,
    35: 10.06,
    40: 10.14,
    45: 10.23,
    50: 10.33,
    55: 10.44,
    60: 10.55,
    65: 10.67,
    70: 10.80,
    75: 10.93,
    80: 11.06,
    85: 11.2,
    90: 11.3,
    95: 11.48,
    100: 11.62,
    105: 11.77,
    110: 11.91,
    115: 12.06,
    120: 12.21,
    125: 12.36,
    130: 12.51,
    135: 12.67,
    140: 12.82,
    145: 12.97,
    150: 13.12,
}

class ForestDomain(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'forest_domain'

    def __str__(self):
        return self.name


class Owner(models.Model):
    name = models.CharField(max_length=100)
    typ = models.CharField(max_length=1, db_column="type")
    forest = models.ForeignKey(ForestDomain)

    class Meta:
        db_table = 'owner'

    def __str__(self):
        return self.name


class Phytosoc(models.Model):
    code = models.CharField(max_length=6)
    description = models.CharField(max_length=200)
    inc_class = models.SmallIntegerField("Ertragsklasse")
    ecol_grp = models.CharField("Ökologische Gruppe", max_length=1)
    bwnatur = models.SmallIntegerField(db_column="BWNATURN_WSL", blank=True, null=True)

    class Meta:
        db_table = 'phytosoc'

    def __str__(self):
        return self.code

EXPO_CHOICES = (
    ('N', "Norden"),
    ('S', "Süden"),
)

class Plot(models.Model):
    nr = models.PositiveIntegerField()
    the_geom = gis_models.PointField(srid=2056)
    phytosoc = models.ForeignKey(Phytosoc, blank=True, null=True, on_delete=models.SET_NULL)
    slope = models.SmallIntegerField("Neigung", null=True, blank=True)
    exposition = models.CharField(max_length=1, choices=EXPO_CHOICES, blank=True, default='')
    sealevel = models.SmallIntegerField("Höhe über Meer (m)")
    igis = models.SmallIntegerField(blank=True, null=True)
    checked = models.BooleanField(default=False)

    class Meta:
        db_table = 'plot'

    def __str__(self):
        return str(self.nr)

    def save(self, *args, **kwargs):
        if self.pk is None and self.phytosoc is None:
            # When creating a plot, try getting phytosoc from the afw database
            self.phytosoc = self.search_phytosoc()
        super(Plot, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('plot_detail', args=[self.pk])

    @property
    def radius(self):
        if not self.slope or self.slope <= 20:
            return 9.77
        rounded = int(round(self.slope/5.0)*5)
        return SLOPE_MAPPING[rounded]

    def search_phytosoc(self):
        """
        Try to get the phytosociology value correponding to this plot from the
        afw database. Return None if not found.
        """
        from django.db import connections
        try:
            cursor = connections['afw'].cursor()
            cursor.execute("SELECT farbwert FROM fp.pflanzensoziologie_laufend "
                           "WHERE ST_Contains(geom, ST_GeomFromText('%s', 2056))" % self.the_geom.wkt)
        except (ProgrammingError, ConnectionDoesNotExist) as err:
            return None
        row = cursor.fetchone()
        if row:
            try:
                return Phytosoc.objects.get(code=row[0])
            except Phytosoc.DoesNotExist:
                return None


# **********************
# plot_obs lookup tables
# **********************

class OwnerType(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_owner_type'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Region(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_region'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Sector(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_sector'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Gap(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_gap'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class DevelStage(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_devel_stage'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class SoilCompaction(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_soil_compaction'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class ForestForm(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_forest_form'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class RegenType(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_regen_type'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class ForestMixture(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_forest_mixture'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class CrownClosure(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_crown_closure'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class StandStructure(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_stand_struct'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Relief(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_relief'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Acidity(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_acidity'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Geology(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_geology'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class PlotObs(models.Model):
    plot = models.ForeignKey(Plot, verbose_name="Aufnahmepunkt (plot)", on_delete=models.CASCADE)
    year = models.SmallIntegerField(verbose_name='Aufnahmejahr', db_index=True)
    inv_period = models.SmallIntegerField(verbose_name='Aufnahmeperiode', null=True)
    owner = models.ForeignKey(Owner, null=True, blank=True, on_delete=models.SET_NULL)
    owner_type = models.ForeignKey(OwnerType, verbose_name="Besitzertyp", null=True, blank=True, on_delete=models.SET_NULL)
    municipality = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', on_delete=models.PROTECT)
    region = models.ForeignKey(Region, null=True, blank=True, on_delete=models.SET_NULL)
    sector = models.ForeignKey(Sector, verbose_name="Sektor", null=True, blank=True, on_delete=models.SET_NULL)
    area = models.SmallIntegerField(null=True, blank=True)
    subsector = models.CharField(max_length=2, blank=True)
    evaluation_unit = models.CharField(max_length=4, blank=True)
    forest_clearing = models.BooleanField(default=False)
    gap = models.ForeignKey(Gap, verbose_name="Blösse", null=True, blank=True, on_delete=models.SET_NULL)
    stand = models.SmallIntegerField(null=True, blank=True)  # To be removed
    stand_devel_stage = models.ForeignKey(DevelStage, verbose_name="Entwicklungsstufe", null=True, blank=True,
        on_delete=models.SET_NULL)
    stand_forest_mixture = models.ForeignKey(ForestMixture, verbose_name="Mischungsgrad",
        related_name="plotobs_stand", null=True, blank=True, on_delete=models.SET_NULL)
    stand_crown_closure = models.ForeignKey(CrownClosure, verbose_name="Schlussgrad",
        related_name="plotobs_stand", null=True, blank=True, on_delete=models.SET_NULL)
    soil_compaction = models.ForeignKey(SoilCompaction, verbose_name="Bodenverdichtung",
        null=True, blank=True, on_delete=models.SET_NULL)
    forest_form = models.ForeignKey(ForestForm, verbose_name="Waldform", null=True, blank=True, on_delete=models.SET_NULL)
    regen_type = models.ForeignKey(RegenType, verbose_name="Verjüngungsart", null=True, blank=True, on_delete=models.SET_NULL)
    # This field is not used, `stand_forest_mixture` contains the trusted data (#2).
    forest_mixture = models.ForeignKey(ForestMixture, null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+')
    # This field is not used, `stand_crown_closure` contains the trusted data (#2).
    crown_closure = models.ForeignKey(CrownClosure, null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+')
    stand_structure = models.ForeignKey(StandStructure, null=True, blank=True, on_delete=models.SET_NULL)
    # 0<val<1 is enforced at database level
    forest_edgef = models.DecimalField("Waldrandfaktor", max_digits=2, decimal_places=1)
    gwl = models.FloatField("Gesamtwuchsleistung (gwl)")
    relief = models.ForeignKey(Relief, null=True, blank=True, on_delete=models.SET_NULL)
    geology = models.ForeignKey(Geology, verbose_name="Geologie", null=True, blank=True, on_delete=models.SET_NULL)
    acidity = models.ForeignKey(Acidity, verbose_name="Azidität", null=True, blank=True, on_delete=models.SET_NULL)
    remarks = models.TextField("Bermerkungen", blank=True)

    # "Unknown" fields
    iprobenr = models.SmallIntegerField()
    ipsi1 = models.IntegerField()
    ipsi2 = models.IntegerField()
    ipsf1 = models.DecimalField(max_digits=10, decimal_places=6)
    ipsf2 = models.DecimalField(max_digits=10, decimal_places=6)
    iflae = models.DecimalField(max_digits=10, decimal_places=6)
    itflae = models.DecimalField(max_digits=10, decimal_places=6)
    ikalter = models.IntegerField()
    ik6 = models.IntegerField(blank=True, null=True)
    ik7 = models.IntegerField(blank=True, null=True)
    ik8 = models.IntegerField(blank=True, null=True)
    ik9 = models.IntegerField(blank=True, null=True)
    ik10 = models.IntegerField(blank=True, null=True)
    izeitpkt = models.CharField(max_length=1, blank=True)
    abt = models.CharField(max_length=10, blank=True)
    ikflag = models.IntegerField()
    ifoa = models.CharField(max_length=5, blank=True)
    irevf = models.IntegerField(blank=True, null=True)
    idistr = models.CharField(max_length=2, blank=True)
    iabt = models.CharField(max_length=10, blank=True)
    iuabt = models.IntegerField(blank=True, null=True)
    iufl = models.IntegerField(blank=True, null=True)
    ibest = models.IntegerField(blank=True, null=True)
    istao = models.CharField(max_length=10, blank=True)
    ia = models.CharField(max_length=1, blank=True)
    ib = models.IntegerField(blank=True, null=True)
    ic = models.CharField(max_length=1, blank=True)
    id_2 = models.CharField(max_length=5, blank=True)
    ie = models.CharField(max_length=2, blank=True)
    isgr = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr2 = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr3 = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr4 = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr5 = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr6 = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr7 = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr8 = models.DecimalField(max_digits=10, decimal_places=6)
    ibgr9 = models.DecimalField(max_digits=10, decimal_places=6)

    class Meta:
        db_table = 'plot_obs'

    def __str__(self):
        return "%s (%s)" % (self.plot.nr, self.year)

    def get_absolute_url(self):
        return reverse('plotobs_detail', args=[self.pk])

    def fix_edge_factor(self):
        """
        When forest edge factor is 0 for an non-clearing plot, it should be
        corrected to 1 (100%).
        """
        if self.forest_edgef == 0 and not self.forest_clearing:
            self.forest_edgef = 1
            self.save()

    def as_geojson(self, dumped=True):
        def to_html(value):
            if value is None:
                return '-'
            elif value is True:
                return 'Ja'
            elif value is False:
                return 'Nein'
            return str(value)

        geojson = {"type": "FeatureCollection", "features": []}
        # Add plot (center), radius, trees
        geojson['features'].append({
            "type": "Feature",
            "geometry": eval(self.plot.the_geom.json),
            "properties": OrderedDict({ "type": "center" }),
        })
        geojson['features'][-1]["properties"]["radius"] = self.plot.radius
        geojson['features'][-1]["properties"].update(
            OrderedDict([(field.verbose_name, to_html(getattr(self, field.name)))
                  for field in self._meta.fields
                  if not field.name.startswith('i') and getattr(self, field.name) not in (None, '')]))
        # Add trees with position
        for treeobs in self.treeobs_set.select_related('tree').all().order_by('tree__nr'):
            geojson['features'].append({
                "type": "Feature",
                "geometry": eval(treeobs.tree.point.json),
                "properties": { "type": "tree", "nr": treeobs.tree.nr },
            })
        if dumped:
            return json.dumps(geojson)
        return geojson


class UpdatedValue(models.Model):
    """
    This table logs updated values, differing fomr those originally imported.
    """
    table_name = models.CharField(max_length=50)
    row_id = models.IntegerField()
    field_name = models.CharField(max_length=50)
    old_value = models.CharField(max_length=255)
    new_value = models.CharField(max_length=255)
    stamp = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(blank=True, default='')

    class Meta:
        db_table = 'updated_value'

    def __str__(self):
        return "%s.%s.%s (%s)" % (self.table_name, self.field_name, self.row_id, self.stamp)

    @classmethod
    def add(self, obj, field_name, old_val, what):
        UpdatedValue.objects.create(
            table_name=obj._meta.db_table, row_id=obj.pk, field_name=field_name,
            old_value=old_val, new_value=getattr(obj, field_name), comment=what
        )

    @classmethod
    def deleted(self, obj, comment):
        UpdatedValue.objects.create(
            table_name=obj._meta.db_table, row_id=obj.pk, field_name='',
            old_value='', new_value='', comment=comment,
        )


class TreeSpecies(models.Model):
    species = models.CharField(max_length=100)
    code = models.SmallIntegerField(blank=True, null=True,
        help_text="National inventory value")
    abbrev = models.CharField(max_length=4, unique=True)
    is_tree = models.BooleanField(default=True, help_text="True if this is a real tree")
    is_special = models.BooleanField(default=False)

    class Meta:
        db_table = 'tree_spec'

    def __str__(self):
        return self.species


class Tree(models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.CASCADE)
    spec = models.ForeignKey(TreeSpecies, blank=True, null=True, on_delete=models.PROTECT)
    nr = models.PositiveSmallIntegerField(help_text='tree number')
    azimuth = models.SmallIntegerField(help_text='angle in Grads')
    distance = models.DecimalField(max_digits=6, decimal_places=2, help_text='[in dm]')

    class Meta:
        db_table = 'tree'

    def __str__(self):
        return "Tree (%s) on plot %s (az:%d dist:%dm)" % (self.spec, self.plot.nr, self.azimuth, self.distance/10)

    @property
    def point(self):
        """
        Calculate and return point from plot origin and relative distance/azimuth.
        """
        grad_to_rad = lambda val: val*math.pi/200
        return Point(self.plot.the_geom.x + float(self.distance/10) * math.sin(grad_to_rad(self.azimuth)),
                     self.plot.the_geom.y + float(self.distance/10) * math.cos(grad_to_rad(self.azimuth)))


class RegenObs(models.Model):
    obs = models.ForeignKey(PlotObs, on_delete=models.CASCADE)
    spec = models.ForeignKey(TreeSpecies, on_delete=models.PROTECT)
    perc = models.DecimalField(max_digits=5, decimal_places=2)

    class Meta:
        db_table = 'regen_obs'

    def __str__(self):
        return "Plot %s (obs %s), %s%% %s" % (self.obs.plot, self.obs, self.perc, self.spec)


# **********************
# tree_obs lookup tables
# **********************

class SurveyType(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_survey_type'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Layer(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    explanation = models.TextField(default='', blank=True)

    class Meta:
        db_table = 'lt_layer'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Rank(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    explanation = models.TextField(default='', blank=True)

    class Meta:
        db_table = 'lt_rank'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Vita(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_vita'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Damage(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_damage'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class DamageCause(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_damage_cause'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class CrownForm(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    explanation = models.TextField(default='', blank=True)

    class Meta:
        db_table = 'lt_crown_form'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class CrownLength(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    explanation = models.TextField(default='', blank=True)

    class Meta:
        db_table = 'lt_crown_len'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class Stem(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_stem'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class TreeObs(models.Model):
    obs = models.ForeignKey(PlotObs, on_delete=models.CASCADE)
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE)
    survey_type = models.ForeignKey(SurveyType, on_delete=models.PROTECT)
    layer = models.ForeignKey(Layer, null=True, blank=True, on_delete=models.SET_NULL)
    dbh = models.SmallIntegerField()
    rank = models.ForeignKey(Rank, null=True, blank=True, on_delete=models.SET_NULL)
    vita = models.ForeignKey(Vita, null=True, blank=True, on_delete=models.SET_NULL)
    damage = models.ForeignKey(Damage, null=True, blank=True, on_delete=models.SET_NULL)
    damage_cause = models.ForeignKey(DamageCause, null=True, blank=True, on_delete=models.SET_NULL)
    crown_form = models.ForeignKey(CrownForm, null=True, blank=True, on_delete=models.SET_NULL)
    crown_length = models.ForeignKey(CrownLength, null=True, blank=True, on_delete=models.SET_NULL)
    stem = models.ForeignKey(Stem, null=True, blank=True, on_delete=models.SET_NULL)

    # Unknown fields
    ianteil = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    iverbiss = models.IntegerField()
    iflag = models.IntegerField()
    ih = models.DecimalField(max_digits=8, decimal_places=6, null=True, blank=True)
    ibon = models.DecimalField(max_digits=8, decimal_places=6, null=True, blank=True)
    ibanr = models.IntegerField()
    ialter = models.IntegerField()
    ibhd2 = models.IntegerField()
    ikreis = models.IntegerField(blank=True, null=True)
    inadelv = models.IntegerField()
    ivergilb = models.IntegerField()
    ikron = models.DecimalField(max_digits=8, decimal_places=6, null=True, blank=True)
    iintens2 = models.CharField(max_length=1, blank=True)
    itoth = models.IntegerField(blank=True, null=True)
    izstam = models.CharField(max_length=1, blank=True)
    ianzahl = models.DecimalField(max_digits=10, decimal_places=6, null=True, blank=True)
    schicht = models.IntegerField()
    iu = models.CharField(max_length=20, blank=True)
    iv = models.IntegerField(blank=True, null=True)
    iw = models.IntegerField(blank=True, null=True)
    iz = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'tree_obs'
        unique_together = [('obs', 'tree')]

    def __str__(self):
        return "Tree obs %s" % (self.id,)


class RegionType(models.Model):
    name = models.CharField(max_length=20)

    class Meta:
        db_table = 'regiontype'

    def __str__(self):
        return self.name


class AdminRegion(models.Model):
    nr = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=100)
    region_type = models.ForeignKey(RegionType, null=True, on_delete=models.PROTECT)
    geom = gis_models.MultiPolygonField(srid=2056, null=True, blank=True)

    class Meta:
        db_table = 'adminregion'

    def __str__(self):
        return "%s %s (%s)" % (self.region_type.name, self.name, self.nr)
