# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0013_treespecies_is_special'),
    ]

    operations = [
        migrations.RenameField(
            model_name='plotobs',
            old_name='radius',
            new_name='iprobenr',
        ),
    ]
