# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0026_add_plotobs_inv_period'),
    ]

    operations = [
        migrations.CreateModel(
            name='Forstkreis',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('nr', models.SmallIntegerField(unique=True)),
                ('name', models.CharField(max_length=20)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=2056, blank=True, null=True)),
            ],
            options={
                'db_table': 'forstkreis',
            },
        ),
    ]
