# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Phytosoc',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=6)),
                ('description', models.CharField(max_length=200)),
                ('inc_class', models.SmallIntegerField()),
                ('ecol_grp', models.CharField(max_length=1)),
            ],
            options={
                'db_table': 'phytosoc',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='plot',
            name='phytosoc',
            field=models.ForeignKey(blank=True, to='observation.Phytosoc', null=True),
            preserve_default=True,
        ),
    ]
