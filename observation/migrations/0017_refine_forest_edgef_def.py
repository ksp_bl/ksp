# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0016_add_updatedvalue_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='forest_edgef',
            field=models.DecimalField(max_digits=2, decimal_places=1),
        ),
        migrations.RunSQL(
            "ALTER TABLE plot_obs ADD CONSTRAINT forest_edgef_limits CHECK (forest_edgef BETWEEN 0 AND 1)",
            "DROP CONSTRAINT forest_edgef_limits"
        ),
    ]
