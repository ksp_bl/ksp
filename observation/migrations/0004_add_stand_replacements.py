# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0003_remove_plotobs_forest_community'),
    ]

    operations = [
        migrations.CreateModel(
            name='DevelStage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_devel_stage',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_crown_closure',
            field=models.ForeignKey(verbose_name='Schlussgrad', blank=True, to='observation.CrownClosure', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_devel_stage',
            field=models.ForeignKey(related_name='plotobs_stand', verbose_name='Entwicklungsstufe', blank=True, to='observation.DevelStage', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_forest_mixture',
            field=models.ForeignKey(related_name='plotobs_stand', verbose_name='Mischungsgrad', blank=True, to='observation.ForestMixture', null=True),
            preserve_default=True,
        ),
    ]
