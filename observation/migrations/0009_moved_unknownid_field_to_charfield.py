# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0008_set_treeobs_layer_null'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='id_2',
            field=models.CharField(max_length=5, blank=True, null=True),
        ),
    ]
