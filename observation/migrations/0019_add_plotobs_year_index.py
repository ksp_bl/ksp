# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0018_add_basic_functions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='year',
            field=models.SmallIntegerField(verbose_name='Jahr', db_index=True),
        ),
    ]
