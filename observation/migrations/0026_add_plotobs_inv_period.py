# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0025_removed_moved_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='inv_period',
            field=models.SmallIntegerField(null=True, verbose_name='Aufnahmeperiode'),
        ),
    ]
