# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0021_add_plot_checked'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='exposition',
            field=models.CharField(choices=[('N', 'Norden'), ('S', 'Süden')], blank=True, default='', max_length=1),
        ),
        migrations.AddField(
            model_name='plot',
            name='sealevel',
            field=models.SmallIntegerField(verbose_name='Höhe über Meer (m)', null=True),
        ),
        migrations.AddField(
            model_name='plot',
            name='slope',
            field=models.SmallIntegerField(blank=True, verbose_name='Neigung', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='subsector',
            field=models.CharField(blank=True, max_length=2),
        ),
    ]
