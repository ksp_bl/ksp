# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def migrate_data(apps, schema_editor):
    Plot = apps.get_model("observation", "Plot")
    for plot in Plot.objects.all().prefetch_related('plotobs_set'):
        values = [po.slope for po in plot.plotobs_set.all()]
        if len(set(values)) > 1:
            print("Plot %s: different values for slope: %s" % (plot.nr, values))
            continue
        else:
            plot.slope = values[0]

        values = [po.exposition for po in plot.plotobs_set.all()]
        if len(set(values)) > 1:
            print("Plot %s: different values for expo: %s" % (plot.nr, values))
            continue
        else:
            plot.exposition = values[0]

        values = [po.sealevel for po in plot.plotobs_set.all()]
        if len(set(values)) > 1:
            print("Plot %s: different values for sealevel: %s" % (plot.nr, values))
            continue
        else:
            plot.sealevel = values[0]
        plot.save()


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0023_reflect_ondelete_fk'),
    ]

    operations = [
        migrations.RunPython(migrate_data),
    ]
