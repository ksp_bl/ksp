# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def convert_null_to_blank(apps, schema_editor):
    PlotObs = apps.get_model("observation", "PlotObs")
    PlotObs.objects.filter(abt__isnull=True).update(abt='')
    PlotObs.objects.filter(iabt__isnull=True).update(iabt='')
    PlotObs.objects.filter(istao__isnull=True).update(istao='')


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0005_moved_some_fields_to_charfield'),
    ]

    operations = [
        migrations.RunPython(
            convert_null_to_blank, lambda x,y: None
        ),
    ]
