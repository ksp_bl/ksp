# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0002_add_phytosoc'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plotobs',
            name='forest_community',
        ),
    ]
