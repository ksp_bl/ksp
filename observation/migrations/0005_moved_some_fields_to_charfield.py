# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0004_add_stand_replacements'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='abt',
            field=models.CharField(max_length=10, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='iabt',
            field=models.CharField(max_length=10, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='istao',
            field=models.CharField(max_length=10, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='stand_crown_closure',
            field=models.ForeignKey(related_name='plotobs_stand', verbose_name='Schlussgrad', blank=True, to='observation.CrownClosure', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='stand_devel_stage',
            field=models.ForeignKey(verbose_name='Entwicklungsstufe', blank=True, to='observation.DevelStage', null=True),
        ),
    ]
