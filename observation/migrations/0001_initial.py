# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gemeinde', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Acidity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_acidity',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CrownClosure',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_crown_closure',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CrownForm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(default='', blank=True)),
            ],
            options={
                'db_table': 'lt_crown_form',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CrownLength',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(default='', blank=True)),
            ],
            options={
                'db_table': 'lt_crown_len',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Damage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_damage',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DamageCause',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_damage_cause',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ForestDomain',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'forest_domain',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ForestForm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_forest_form',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ForestMixture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_forest_mixture',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Gap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_gap',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Geology',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_geology',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Layer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(default='', blank=True)),
            ],
            options={
                'db_table': 'lt_layer',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('typ', models.CharField(max_length=1, db_column='type')),
                ('forest', models.ForeignKey(to='observation.ForestDomain')),
            ],
            options={
                'db_table': 'owner',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OwnerType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_owner_type',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nr', models.PositiveIntegerField()),
                ('the_geom', django.contrib.gis.db.models.fields.PointField(srid=2056)),
                ('igis', models.SmallIntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'plot',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlotObs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('radius', models.SmallIntegerField()),
                ('slope', models.SmallIntegerField(null=True, blank=True)),
                ('exposition', models.CharField(default='', max_length=1, blank=True, choices=[('N', 'Norden'), ('S', 'S\xfcden')])),
                ('sealevel', models.SmallIntegerField()),
                ('area', models.SmallIntegerField(null=True, blank=True)),
                ('subsector', models.CharField(max_length=2)),
                ('evaluation_unit', models.CharField(max_length=4, blank=True)),
                ('forest_clearing', models.BooleanField(default=False)),
                ('stand', models.SmallIntegerField(null=True, blank=True)),
                ('forest_community', models.CharField(max_length=5)),
                ('forest_edgef', models.DecimalField(max_digits=6, decimal_places=4)),
                ('gwl', models.FloatField()),
                ('remarks', models.TextField()),
                ('ipsi1', models.IntegerField()),
                ('ipsi2', models.IntegerField()),
                ('ipsf1', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ipsf2', models.DecimalField(max_digits=10, decimal_places=6)),
                ('iflae', models.DecimalField(max_digits=10, decimal_places=6)),
                ('itflae', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ikalter', models.IntegerField()),
                ('ik6', models.IntegerField(null=True, blank=True)),
                ('ik7', models.IntegerField(null=True, blank=True)),
                ('ik8', models.IntegerField(null=True, blank=True)),
                ('ik9', models.IntegerField(null=True, blank=True)),
                ('ik10', models.IntegerField(null=True, blank=True)),
                ('izeitpkt', models.CharField(max_length=1, blank=True)),
                ('abt', models.IntegerField(null=True, blank=True)),
                ('ikflag', models.IntegerField()),
                ('ifoa', models.CharField(max_length=5, blank=True)),
                ('irevf', models.IntegerField(null=True, blank=True)),
                ('idistr', models.CharField(max_length=2, blank=True)),
                ('iabt', models.IntegerField(null=True, blank=True)),
                ('iuabt', models.IntegerField(null=True, blank=True)),
                ('iufl', models.IntegerField(null=True, blank=True)),
                ('ibest', models.IntegerField(null=True, blank=True)),
                ('istao', models.IntegerField(null=True, blank=True)),
                ('ia', models.CharField(max_length=1, blank=True)),
                ('ib', models.IntegerField(null=True, blank=True)),
                ('ic', models.CharField(max_length=1, blank=True)),
                ('id_2', models.IntegerField(null=True, blank=True)),
                ('ie', models.CharField(max_length=2, blank=True)),
                ('isgr', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr2', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr3', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr4', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr5', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr6', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr7', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr8', models.DecimalField(max_digits=10, decimal_places=6)),
                ('ibgr9', models.DecimalField(max_digits=10, decimal_places=6)),
                ('acidity', models.ForeignKey(blank=True, to='observation.Acidity', null=True)),
                ('crown_closure', models.ForeignKey(blank=True, to='observation.CrownClosure', null=True)),
                ('forest_form', models.ForeignKey(blank=True, to='observation.ForestForm', null=True)),
                ('forest_mixture', models.ForeignKey(blank=True, to='observation.ForestMixture', null=True)),
                ('gap', models.ForeignKey(blank=True, to='observation.Gap', null=True)),
                ('geology', models.ForeignKey(blank=True, to='observation.Geology', null=True)),
                ('municipality', models.ForeignKey(to='gemeinde.Gemeinde', verbose_name='Gemeinde')),
                ('owner', models.ForeignKey(blank=True, to='observation.Owner', null=True)),
                ('owner_type', models.ForeignKey(blank=True, to='observation.OwnerType', null=True)),
                ('plot', models.ForeignKey(to='observation.Plot')),
            ],
            options={
                'db_table': 'plot_obs',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rank',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(default='', blank=True)),
            ],
            options={
                'db_table': 'lt_rank',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegenObs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('perc', models.DecimalField(max_digits=5, decimal_places=2)),
                ('obs', models.ForeignKey(to='observation.PlotObs')),
            ],
            options={
                'db_table': 'regen_obs',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegenType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_regen_type',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_region',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relief',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_relief',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_sector',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SoilCompaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_soil_compaction',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StandStructure',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_stand_struct',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Stem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_stem',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SurveyType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_survey_type',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nr', models.PositiveSmallIntegerField(help_text='tree number')),
                ('azimuth', models.SmallIntegerField(help_text='angle in Grads')),
                ('distance', models.DecimalField(help_text='[in dm]', max_digits=6, decimal_places=2)),
                ('plot', models.ForeignKey(to='observation.Plot')),
            ],
            options={
                'db_table': 'tree',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TreeObs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dbh', models.SmallIntegerField()),
                ('ianteil', models.DecimalField(null=True, max_digits=10, decimal_places=6, blank=True)),
                ('iverbiss', models.IntegerField()),
                ('iflag', models.IntegerField()),
                ('ih', models.DecimalField(null=True, max_digits=8, decimal_places=6, blank=True)),
                ('ibon', models.DecimalField(null=True, max_digits=8, decimal_places=6, blank=True)),
                ('ibanr', models.IntegerField()),
                ('ialter', models.IntegerField()),
                ('ibhd2', models.IntegerField()),
                ('ikreis', models.IntegerField(null=True, blank=True)),
                ('inadelv', models.IntegerField()),
                ('ivergilb', models.IntegerField()),
                ('ikron', models.DecimalField(null=True, max_digits=8, decimal_places=6, blank=True)),
                ('iintens2', models.CharField(max_length=1, blank=True)),
                ('itoth', models.IntegerField(null=True, blank=True)),
                ('izstam', models.CharField(max_length=1, blank=True)),
                ('ianzahl', models.DecimalField(null=True, max_digits=10, decimal_places=6, blank=True)),
                ('schicht', models.IntegerField()),
                ('iu', models.CharField(max_length=20, blank=True)),
                ('iv', models.IntegerField(null=True, blank=True)),
                ('iw', models.IntegerField(null=True, blank=True)),
                ('iz', models.IntegerField(null=True, blank=True)),
                ('crown_form', models.ForeignKey(blank=True, to='observation.CrownForm', null=True)),
                ('crown_length', models.ForeignKey(blank=True, to='observation.CrownLength', null=True)),
                ('damage', models.ForeignKey(blank=True, to='observation.Damage', null=True)),
                ('damage_cause', models.ForeignKey(blank=True, to='observation.DamageCause', null=True)),
                ('layer', models.ForeignKey(to='observation.Layer')),
                ('obs', models.ForeignKey(to='observation.PlotObs')),
                ('rank', models.ForeignKey(blank=True, to='observation.Rank', null=True)),
                ('stem', models.ForeignKey(blank=True, to='observation.Stem', null=True)),
                ('survey_type', models.ForeignKey(to='observation.SurveyType')),
                ('tree', models.ForeignKey(to='observation.Tree')),
            ],
            options={
                'db_table': 'tree_obs',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TreeSpecies',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('species', models.CharField(max_length=100)),
                ('code', models.SmallIntegerField(help_text='National inventory value', null=True, blank=True)),
                ('abbrev', models.CharField(unique=True, max_length=4)),
                ('is_tree', models.BooleanField(default=True, help_text='True if this is a real tree')),
            ],
            options={
                'db_table': 'tree_spec',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vita',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_vita',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='vita',
            field=models.ForeignKey(blank=True, to='observation.Vita', null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='treeobs',
            unique_together=set([('obs', 'tree')]),
        ),
        migrations.AddField(
            model_name='tree',
            name='spec',
            field=models.ForeignKey(blank=True, to='observation.TreeSpecies', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='regenobs',
            name='spec',
            field=models.ForeignKey(to='observation.TreeSpecies'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='regen_type',
            field=models.ForeignKey(blank=True, to='observation.RegenType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='region',
            field=models.ForeignKey(blank=True, to='observation.Region', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='relief',
            field=models.ForeignKey(blank=True, to='observation.Relief', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='sector',
            field=models.ForeignKey(blank=True, to='observation.Sector', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='soil_compaction',
            field=models.ForeignKey(blank=True, to='observation.SoilCompaction', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_structure',
            field=models.ForeignKey(blank=True, to='observation.StandStructure', null=True),
            preserve_default=True,
        ),
    ]
