# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def convert_null_to_blank(apps, schema_editor):
    PlotObs = apps.get_model("observation", "PlotObs")
    PlotObs.objects.filter(id_2__isnull=True).update(id_2='')


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0009_moved_unknownid_field_to_charfield'),
    ]

    operations = [
        migrations.RunPython(
            convert_null_to_blank, lambda x,y: None
        ),
    ]
