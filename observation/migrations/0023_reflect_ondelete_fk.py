# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0022_moved_plotobs_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plot',
            name='phytosoc',
            field=models.ForeignKey(to='observation.Phytosoc', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='acidity',
            field=models.ForeignKey(verbose_name='Azidität', blank=True, null=True, to='observation.Acidity', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='crown_closure',
            field=models.ForeignKey(to='observation.CrownClosure', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='forest_form',
            field=models.ForeignKey(verbose_name='Waldform', blank=True, null=True, to='observation.ForestForm', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='forest_mixture',
            field=models.ForeignKey(to='observation.ForestMixture', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gap',
            field=models.ForeignKey(verbose_name='Blösse', blank=True, null=True, to='observation.Gap', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='geology',
            field=models.ForeignKey(verbose_name='Geologie', blank=True, null=True, to='observation.Geology', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='municipality',
            field=models.ForeignKey(verbose_name='Gemeinde', to='gemeinde.Gemeinde', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='owner',
            field=models.ForeignKey(to='observation.Owner', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='owner_type',
            field=models.ForeignKey(verbose_name='Besitzertyp', blank=True, null=True, to='observation.OwnerType', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='regen_type',
            field=models.ForeignKey(verbose_name='Verjüngungsart', blank=True, null=True, to='observation.RegenType', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='region',
            field=models.ForeignKey(to='observation.Region', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='relief',
            field=models.ForeignKey(to='observation.Relief', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='sector',
            field=models.ForeignKey(verbose_name='Sektor', blank=True, null=True, to='observation.Sector', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='soil_compaction',
            field=models.ForeignKey(verbose_name='Bodenverdichtung', blank=True, null=True, to='observation.SoilCompaction', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='stand_crown_closure',
            field=models.ForeignKey(related_name='plotobs_stand', verbose_name='Schlussgrad', blank=True, null=True, to='observation.CrownClosure', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='stand_devel_stage',
            field=models.ForeignKey(verbose_name='Entwicklungsstufe', blank=True, null=True, to='observation.DevelStage', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='stand_forest_mixture',
            field=models.ForeignKey(related_name='plotobs_stand', verbose_name='Mischungsgrad', blank=True, null=True, to='observation.ForestMixture', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='stand_structure',
            field=models.ForeignKey(to='observation.StandStructure', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='regenobs',
            name='spec',
            field=models.ForeignKey(to='observation.TreeSpecies', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='tree',
            name='spec',
            field=models.ForeignKey(to='observation.TreeSpecies', blank=True, null=True, on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='crown_form',
            field=models.ForeignKey(to='observation.CrownForm', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='crown_length',
            field=models.ForeignKey(to='observation.CrownLength', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='damage',
            field=models.ForeignKey(to='observation.Damage', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='damage_cause',
            field=models.ForeignKey(to='observation.DamageCause', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='layer',
            field=models.ForeignKey(to='observation.Layer', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='rank',
            field=models.ForeignKey(to='observation.Rank', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='stem',
            field=models.ForeignKey(to='observation.Stem', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='survey_type',
            field=models.ForeignKey(to='observation.SurveyType', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='treeobs',
            name='vita',
            field=models.ForeignKey(to='observation.Vita', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
    ]
