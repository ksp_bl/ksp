# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0020_plotobs_verbose_names'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='checked',
            field=models.BooleanField(default=False),
        ),
    ]
