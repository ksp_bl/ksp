# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0015_add_unmanaged_views'),
    ]

    operations = [
        migrations.CreateModel(
            name='UpdatedValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('table_name', models.CharField(max_length=50)),
                ('row_id', models.IntegerField()),
                ('field_name', models.CharField(max_length=50)),
                ('old_value', models.CharField(max_length=255)),
                ('new_value', models.CharField(max_length=255)),
                ('stamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(default='', blank=True)),
            ],
            options={
                'db_table': 'updated_value',
            },
        ),
    ]
