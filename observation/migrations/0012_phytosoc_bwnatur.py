# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0011_set_charfield_to_notnull'),
    ]

    operations = [
        migrations.AddField(
            model_name='phytosoc',
            name='bwnatur',
            field=models.SmallIntegerField(null=True, db_column='BWNATURN_WSL', blank=True),
            preserve_default=True,
        ),
    ]
