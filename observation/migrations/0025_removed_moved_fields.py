# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0024_migrate_data_to_plot'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plotobs',
            name='exposition',
        ),
        migrations.RemoveField(
            model_name='plotobs',
            name='sealevel',
        ),
        migrations.RemoveField(
            model_name='plotobs',
            name='slope',
        ),
        migrations.AlterField(
            model_name='plot',
            name='sealevel',
            field=models.SmallIntegerField(verbose_name='Höhe über Meer (m)'),
        ),
    ]
