# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0017_refine_forest_edgef_def'),
    ]

    operations = [
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION ksp_lokale_dichte(edgef numeric) RETURNS numeric AS $$
SELECT CASE WHEN $1=1 THEN 100.0 / 3.0
            WHEN $1=0 THEN 0
            ELSE 100.0 / 3.0 * 1 / $1
       END;
$$ LANGUAGE SQL;""",
            "DROP FUNCTION ksp_lokale_dichte(edgef numeric)"),


        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_volume_ha(diameter smallint, dichte numeric) RETURNS numeric AS $$
  SELECT CASE WHEN $1 = 0 THEN 0 ELSE (($1 * $1 * 0.0011) - (@$1 * 0.0011) - 0.079) * $2 END AS volume_ha;
$$ LANGUAGE SQL;""",
            "DROP FUNCTION ksp_volume_ha(diameter smallint, dichte numeric)"),


        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_grundflaeche_ha(diameter smallint, dichte numeric) RETURNS float AS $$
  SELECT pi() * $1 * $1 / 10000::float * $2;
$$ LANGUAGE SQL;""",
            "DROP FUNCTION ksp_grundflaeche_ha(diameter smallint, dichte numeric)"),
    ]
