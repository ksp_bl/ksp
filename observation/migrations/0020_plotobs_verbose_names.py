# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0019_add_plotobs_year_index'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='acidity',
            field=models.ForeignKey(verbose_name='Azidität', blank=True, to='observation.Acidity', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='forest_edgef',
            field=models.DecimalField(verbose_name='Waldrandfaktor', max_digits=2, decimal_places=1),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gap',
            field=models.ForeignKey(verbose_name='Blösse', blank=True, to='observation.Gap', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='geology',
            field=models.ForeignKey(verbose_name='Geologie', blank=True, to='observation.Geology', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gwl',
            field=models.FloatField(verbose_name='Gesamtwuchsleistung (gwl)'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='owner_type',
            field=models.ForeignKey(verbose_name='Besitzer', blank=True, to='observation.OwnerType', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='plot',
            field=models.ForeignKey(verbose_name='Aufnahmepunkt (plot)', to='observation.Plot'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='sealevel',
            field=models.SmallIntegerField(verbose_name='Höhe über Meer (m)'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='sector',
            field=models.ForeignKey(verbose_name='Sektor', blank=True, to='observation.Sector', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='slope',
            field=models.SmallIntegerField(null=True, verbose_name='Neigung', blank=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='year',
            field=models.SmallIntegerField(verbose_name='Aufnahmejahr', db_index=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='forest_form',
            field=models.ForeignKey(verbose_name='Waldform', blank=True, to='observation.ForestForm', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='regen_type',
            field=models.ForeignKey(verbose_name='Verjüngungsart', blank=True, to='observation.RegenType', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='soil_compaction',
            field=models.ForeignKey(verbose_name='Bodenverdichtung', blank=True, to='observation.SoilCompaction', null=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='remarks',
            field=models.TextField(verbose_name='Bermerkungen', blank=True),
        ),

    ]
