# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0007_set_charfields_to_notnull'),
    ]

    operations = [
        migrations.AlterField(
            model_name='treeobs',
            name='layer',
            field=models.ForeignKey(blank=True, to='observation.Layer', null=True),
        ),
    ]
