# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0006_set_null_to_blank'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='abt',
            field=models.CharField(max_length=10, blank=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='iabt',
            field=models.CharField(max_length=10, blank=True),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='istao',
            field=models.CharField(max_length=10, blank=True),
        ),
    ]
