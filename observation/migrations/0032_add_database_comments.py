# Generated by Django 1.9.1 on 2016-02-04 07:31
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0031_adminregion_nr_char'),
    ]

    operations = [
        migrations.RunSQL(
            "COMMENT ON COLUMN plot_obs.forest_mixture_id IS 'Unused. "
            "See the field plot_obs.stand_forest_mixture_id instead.';"),
        migrations.RunSQL(
            "COMMENT ON COLUMN plot_obs.crown_closure_id IS 'Unused. "
            "See the field plot_obs.stand_crown_closure_id instead.';"),
    ]
