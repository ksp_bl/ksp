# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0012_phytosoc_bwnatur'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='is_special',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
