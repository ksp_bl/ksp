# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0014_rename_radius_to_iprobenr'),
    ]

    operations = [
        migrations.CreateModel(
            name='Biotopwert',
            fields=[
                ('id', models.OneToOneField(primary_key=True, db_column='id', serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('biolfi1m', models.FloatField(verbose_name='Biotopwert (BIOLFI1M)', db_column='BIOLFI1M')),
                ('bioklass', models.SmallIntegerField(verbose_name='Biotopwert Klasse', db_column='Biotopwert Klassen')),
            ],
            options={
                'db_table': 'Biotopwert Klassen pro plot_obs, year, owner_type_id',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BWARTEN',
            fields=[
                ('obs', models.OneToOneField(primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('bwarten', models.IntegerField(verbose_name='Geh\xf6lzartenvielfalt (BWARTEN)', db_column='BWARTEN')),
                ('num_spec', models.IntegerField(verbose_name='Anzahl Baumarten', db_column='Anzahl Baumarten')),
                ('special_spec', models.IntegerField(verbose_name='Special Species', db_column='Special Species')),
            ],
            options={
                'db_table': 'BWARTEN - Anzahl Baumarten - Special Species',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BWNATURN',
            fields=[
                ('obs', models.OneToOneField(primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('code', models.CharField(max_length=6)),
                ('bwnaturn', models.IntegerField(verbose_name='Naturn\xe4he des Nadelholzanteils (BWNATURN)', db_column='BWNATURN')),
                ('fichte_perc', models.FloatField(verbose_name='% Basalfl\xe4chenanteil der Fichte', db_column='%% Basalfl\xe4chenanteil der Fichte')),
                ('nadel_perc', models.FloatField(verbose_name='% Basalfl\xe4chenanteil Nadelholz', db_column='%% Basalfl\xe4chenanteil Nadelholz')),
                ('nadel_wtanne_perc', models.FloatField(verbose_name='% Basalfl\xe4chenanteil Nadelholz ohne Tanne', db_column='%% Basalfl\xe4chenanteil Nadelholz ohne Tanne')),
            ],
            options={
                'db_table': 'BWNATURN - %% Basalfl\xe4che Ndh, Fi Ndh ohne Ta',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BWSTRU1M',
            fields=[
                ('id', models.OneToOneField(primary_key=True, db_column='id', serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('bwstru1m', models.IntegerField(verbose_name='Strukturvielfalt (BWSTRU1M)', db_column='BWSTRU1M')),
            ],
            options={
                'db_table': 'BWSTRU1M - ',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HolzProduktion',
            fields=[
                ('id', models.OneToOneField(primary_key=True, db_column='id', serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('stammzahl_ha', models.FloatField(verbose_name='Stammzahl/ha', db_column='Stammzahl/ha')),
                ('anzahl_baume', models.FloatField(verbose_name='Anzahl B\xe4ume', db_column='Anzahl B\xe4ume absolut')),
                ('volumen', models.FloatField(verbose_name='Volumen (m3/ha)', db_column='Volumen (m3/ha)')),
                ('grundflaeche', models.FloatField(verbose_name='Grundfl\xe4che (m2/ha)', db_column='Grundfl\xe4che (m2/ha)')),
            ],
            options={
                'db_table': 'Analyse pro Baumart, Plot und jahr:Stammzahl, Volumen, Grundfl',
                'managed': False,
            },
        ),
    ]
