// Missing IE indexOf for arrays
if (!Array.prototype.indexOf) {
   Array.prototype.indexOf = function(item) {
      var i = this.length;
      while (i--) {
         if (this[i] === item) return i;
      }
      return -1;
   }
}

$(document).ready(function() {
    $('div.togglable').prepend('<img class="tri" src="' + static_url + 'img/tri-closed-white.png' + '">');
    $('div.togglable').click(function (ev) {
        var target = $(this).next();
        if (target.is(':visible')) {
            $('img.tri', this).attr('src', static_url + 'img/tri-closed-white.png');
        } else {
            $('img.tri', this).attr('src', static_url + 'img/tri-open-white.png');
        }
        target.toggle();
    });
    $('select[name="inventory_filter"]').change(function(ev) {
        window.location.href = '?aufn=' + $(this).val();
    });
    $(document).tooltip();
});

