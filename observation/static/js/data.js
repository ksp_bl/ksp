// From blue to red
//var colors = ['#0006E5', '#0058E7', '#00ACE9', '#00ECD5', '#00EE82', '#00F02D', '#28F300', '#80F500', '#DAF700', '#FABE00', '#FC6600', '#FF0B00'];
// Levels of red
var colors = ['#FFD6D4', '#FFB2AE', '#FF8D87', '#FF6961', '#D93A32', '#B2170E', '#8C0700'];

/* Default styles */
var fill = new ol.style.Fill({
    color: 'rgba(255,255,255,0.4)'
});
var stroke = new ol.style.Stroke({
    color: '#3399CC',
    width: 1.25
});
var styles = [
    new ol.style.Style({
      image: new ol.style.Circle({
        fill: fill,
        stroke: stroke,
        radius: 5
      }),
      fill: fill,
      stroke: stroke
    })
];


function mapObject() {
    this.form_data = null;
    this.proj_2056 = ol.proj.get('EPSG:2056');
    this.baselayer = new ol.layer.Tile({
        source: new ol.source.TileWMS({
          url: 'http://geowms.bl.ch',
          params: {
            'LAYERS': 'orthofotos_agi_2015_group',
            'FORMAT': 'image/png'
          },
        })
    });
    this.map = new ol.Map({
        target: 'map',
        layers: [
          this.baselayer
        ],
        view: new ol.View({
          projection: this.proj_2056,
          center: [2614294, 1260540],
          zoom: 11
        })
    });
    this.vector_layer = null;
    // Build styles for color levels
    this.color_styles = [];
    for (var i=0; i<colors.length; i++) {
        var cfill = new ol.style.Fill({color: colors[i]});
        this.color_styles.push(new ol.style.Style({
          image: new ol.style.Circle({
            fill: cfill,
            radius: 5
          }),
          fill: cfill,
          stroke: stroke
        }));
    }
}

mapObject.prototype.addLayer = function(form_data) {
    form_data += '&format=json';
    // Check that no aggregation is in effect
    if (form_data.indexOf('aggr=') >= 0) {
        $('div#client_error').html("Gruppiert Daten können auf der Karte nicht angezeigt werden").show();
        $('img#wait').hide();
        return;
    } else $('div#client_error').hide();

    if (this.vector_layer != null) this.map.removeLayer(this.vector_layer);
    this.vector_layer = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: $("#dataform").attr('action') + "?" + form_data,
            format: new ol.format.GeoJSON()
        })
    })
    this.map.addLayer(this.vector_layer);
    self = this;
    this.vector_layer.once("change", function(event) {
        // Zoom to vector layer points
        self.map.getView().fitExtent(self.vector_layer.getSource().getExtent(), (self.map.getSize()));
        // Extract and display column names from metadata request
        var cols = [];
        $.each(metadata['map_color_fields'], function(idx, el) {
            cols.push('<li><input type="radio" id="disp_'+el[0]+'" name="cols" value="'+el[1]+'"> <label for="disp_'+el[0]+'">'+el[1]+'</label></li>');
        });
        $('ul#cols').html(cols.join(''));
        $('img#wait').hide();
    });
    this.form_data = form_data;
    this.vector_layer.styled_with = 'default';
};

mapObject.prototype.styleMap = function(prop_key) {
    // Find all property values matching prop_key and color features accordingly
    var minim; var maxim;
    $.each(this.vector_layer.getSource().getFeatures(), function(idx, feature) {
        var value = parseFloat(feature.get(prop_key));
        if (isNaN(value)) return;
        if (typeof minim == "undefined") { minim = maxim = value; }
        else {
            minim = Math.min(minim, value);
            maxim = Math.max(maxim, value);
        }
    });
    var gap = maxim - minim;
    self = this;
    this.vector_layer.setStyle(function(feature, resolution) {
        var value = parseFloat(feature.get(prop_key));
        if (!isNaN(value)) {
            col_idx = Math.round((value - minim)/gap * (colors.length-1));
            return [self.color_styles[col_idx]];
        }
        else return styles;
    });
    this.vector_layer.styled_with = prop_key;
};

function populate_grouping() {
    // Dynamically populate grouping checkboxes depending on chosen views
    $('div#grouping').html('');
    if ($('input[name=bio]:checked').val()) {
        // Year, inv_period and perimeter-related are always present
        $('div#grouping').append(
            '<input id="cb_municipality" type="radio" name="paggr" value="municipality"> <label for="cb_municipality">Gemeinde</label><br>' +
            '<input id="cb_forstkreis" type="radio" name="paggr" value="forstkreis"> <label for="cb_forstkreis">Forstkreis</label><br>' +
            '<input id="cb_forstrevier" type="radio" name="paggr" value="forstrevier"> <label for="cb_forstrevier">Forstrevier</label><br>' +
            '<input id="cb_jagdrevier" type="radio" name="paggr" value="jagdrevier"> <label for="cb_jagdrevier">Jagdrevier</label><br>' +
            '<input id="cb_wep" type="radio" name="paggr" value="WEP"> <label for="cb_wep">WEP</label><br>' +
            '<input id="cb_eigentumer" type="radio" name="paggr" value="eigentümer"> <label for="cb_eigentumer">Eigentümer</label><br><br>' +
            '<input id="cb_year" type="radio" name="yaggr" value="year" checked> <label for="cb_year">Jahr</label><br>' +
            '<input id="cb_inv_period" type="radio" name="yaggr" value="inv_period"> <label for="cb_inv_period">Aufnahmeperiode</label><br><br>'
        );

        // Specific grouping depending on the chosen view
        $.each($("input[name=bio]:checked"), function() {
            var model = views[$(this).val()];
            var keys = model['groupables'];
            for (i=0; i<keys.length; i++) {
                var label = model['model'][keys[i]];
                if (label === undefined) {
                    if (keys[i] == 'spec') label = "Baumart";
                    else label = views['plotobs']['model'][keys[i]];
                }
                $('div#grouping').append('<input id="cb_'+keys[i]+'" type="checkbox" name="aggr" value="'+keys[i]+'"> <label for="cb_'+keys[i]+'">'+label+'</label><br>');
            }
        });

        // Phytosociology-related
        $('div#grouping').append(
            '<input id="cb_phyto_code" type="checkbox" name="aggr" value="phyto_code"> <label for="cb_phyto_code">Phytosoziologie</label><br>' +
            '<input id="cb_inc_class" type="checkbox" name="aggr" value="inc_class"> <label for="cb_inc_class">Ertragsklasse</label><br>' +
            '<input id="cb_ecol_grp" type="checkbox" name="aggr" value="ecol_grp"> <label for="cb_ecol_grp">Ökologische Gruppe</label><br>'
        );

        $('div#grouping').append('<hr><!--input id="cb_stddev" type="checkbox" name="stddev" value="1"--> <label for="cb_stddev"><i>Mit Standardabweichung (in Bearb.)</i></label>');

        // Allow deselecting radio buttons
        $('div#grouping input[name=paggr]').click(function() {
            if($(this).data('previousValue') == true){
                $(this).prop('checked', false);
            } else {
                $('input[name=paggr]').data('previousValue', false);
            }
            $(this).data('previousValue', $(this).prop('checked'));
        });
    }
}

function highlight_perimeter_choices(choice_groups) {
    $.each(choice_groups, function(idx, val) {
        var num_checked = $(choice_groups[idx]).find('input:checked').length;
        var group = $(choice_groups[idx]);
        if (num_checked > 0) {
            group.addClass('selected');
            group.find('span.num_choices').html(num_checked);
            group.find('img.clear_choices').show();
        } else {
            group.removeClass('selected');
            group.find('span.num_choices').html('');
            group.find('img.clear_choices').hide();
        }
    });
}

function filter_choices(container, value) {
    // With a value coming from a text input, this allows filtering lengthy
    // checkboxes lists.
    var search_regex = new RegExp("(" + value + ")", "gi");
    if (value) {
        var cbs = $(container).find('input[type=checkbox]');
        $.each(cbs, function(idx, val) {
            var cont_div = $(this.parentNode);
            if (cont_div.text().match(search_regex) || $(this).is(':checked')) {
                if (!cont_div.is(':visible')) cont_div.show();
            } else cont_div.hide();
        });
    }
    else {
        $(container).find(':hidden').show();
    }
    return false;
}

var map = null;
var data = null;
var form_data = null;
var metadata = null;
$(document).ready(function() {
    $(":button", "div#refresh").click(function (ev) {
        $('img#wait').show();
        form_data = $("#dataform").serialize();
        $.getJSON($("#dataform").attr('action'), form_data + '&metadata=1&format=json', function(data) {
            metadata = data;
        });
        if ($('li#data-tab').hasClass('active')) {
            $("div#datamain").load($("#dataform").attr('action'), form_data, function(response, status, xhr) {
                $('img#wait').hide();
                if (status == "error") $('div#server_error').show();
                else {
                    $('div#server_error').hide();
                    $("table.tablesorter").tablesorter({
                        sortList: [[0,0]]
                        //widgets: ["filter"]
                    });
                }
            });
        } else {
            map.addLayer(form_data);
        }
    });
    $("#csv-export").click(function (ev) {
        ev.preventDefault();
        form_data = $("#dataform").serialize();
        window.location.href = $("#dataform").attr('action') + '?format=csv&' + form_data;
    });
    $('li.tab').click(function(ev) {
        if ($(this).hasClass('active')) return false;
        $('li.tab').removeClass('active');
        $(this).addClass('active');
        $('div#mapmain, div#datamain').toggle();
        if (this.id == 'map-tab') {
            if (map == null) {
                map = new mapObject();
            }
            if (form_data != null && map.form_data != form_data+'&format=json') {
                // Load data into the map
                map.addLayer(form_data);
            }
        } else {
            $('div#client_error').hide();
        }
    });
    $('ul#cols').on('click', 'input', function (ev) {
        if (map.vector_layer.styled_with == this.value) {
            map.vector_layer.setStyle();
            map.vector_layer.styled_with = 'default';
            $(this).prop('checked', false);
        } else map.styleMap(this.value);
    });
    $('img.clear_choices').click(function(ev) {
        ev.stopPropagation();
        $(this).parent().next().find('input:checked').prop('checked', false);
        highlight_perimeter_choices($(this).closest('div.perim_choice'));
    });

    $('input[name=bio]').click(function () {
        $('div.view-details').hide();
        $('div#details-' + $(this).attr('id')).show();
        populate_grouping();
    });
    populate_grouping();

    $('div.perim_choice input').change(function() {
        highlight_perimeter_choices($(this).closest('div.perim_choice'));
    });
    highlight_perimeter_choices($('div.perim_choice'));

    // Delay when user type in choice filter input
    var timer;
    $("input.choice_filter").keyup(function() {
        clearTimeout(timer);
        var ms = 1000;
        var val = this.value;
        var parent = this.parentNode;
        timer = setTimeout(function() {
            filter_choices(parent, val);
        }, ms);
    });
});
