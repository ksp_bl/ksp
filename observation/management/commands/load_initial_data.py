from django.core.management import BaseCommand, call_command

from observation.models import OBSERVATION_FIXTURES


class Command(BaseCommand):
    fixtures = ('gemeinde.json',) + OBSERVATION_FIXTURES + ('phytosoc.json',)

    def handle(self, *args, **kwargs):
        for data_file in self.fixtures:
            call_command('loaddata', data_file)
