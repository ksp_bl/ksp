from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry
from django.core.management import BaseCommand, CommandError
from django.db import connections

from observation.models import AdminRegion, RegionType


class Command(BaseCommand):
    """
    Populate the AdminRegion model with data from the afw database.
    """
    def add_arguments(self, parser):
        parser.add_argument("--region",
            choices=['forstkreis', 'forstrevier', 'jagdrevier', 'wep', 'eigentumer'],
            action='append',
            help="Choose the region(s) to import from the afw database.")

    def handle(self, *args, **options):
        if not 'afw' in settings.DATABASES:
            raise CommandError("The afw database is not available in settings.")
        self.cursor = connections['afw'].cursor()

        if 'forstkreis' in options['region'] or not options['region']:
            self.import_forstkreis()
        if 'forstrevier' in options['region'] or not options['region']:
            self.import_forstrevier()
        if 'jagdrevier' in options['region'] or not options['region']:
            self.import_jagdrevier()
        if 'wep' in options['region'] or not options['region']:
            self.import_wep()
        if 'eigentumer' in options['region'] or not options['region']:
            self.import_eigentumer()
        self.cursor.close()

        cursor = connections['default'].cursor()
        cursor.execute('''REFRESH MATERIALIZED VIEW plot_in_region;''')
        cursor.close()

    def import_forstkreis(self):
        self.stdout.write("Importing Forstkreis objects...")
        self.cursor.execute(
            '''SELECT lut_code, name FROM "common"."lut_forstkreis" ORDER BY lut_code''')
        rtype, _ = RegionType.objects.get_or_create(name='Forstkreis')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for row in self.cursor.fetchall():
            lut_code, name = row
            if lut_code < 1:
                continue
            fkreis, created = AdminRegion.objects.get_or_create(nr=lut_code, name=name, region_type=rtype)
            self.cursor.execute(
                '''SELECT ST_Multi(ST_Union(f.geom)) FROM admin.admin_einteilung_blbs AS f WHERE f.forstkreis=%s GROUP BY f.forstkreis''',
                [lut_code]
            )
            geom = GEOSGeometry(self.cursor.fetchone()[0])
            fkreis.geom = geom
            fkreis.save()

    def import_forstrevier(self):
        self.stdout.write("Importing Forstrevier objects...")
        self.cursor.execute(
            '''SELECT lut_code, name, nr FROM "common"."lut_forstrevier" ORDER BY lut_code''')
        rtype, _ = RegionType.objects.get_or_create(name='Forstrevier')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for row in self.cursor.fetchall():
            lut_code, name, nr = row
            if lut_code < 1:
                continue
            frevier, created = AdminRegion.objects.get_or_create(nr=nr or '', name=name, region_type=rtype)
            self.cursor.execute(
                '''SELECT ST_Multi(ST_Union(f.geom)) FROM admin.admin_einteilung_blbs AS f WHERE f.forstrevier=%s GROUP BY f.forstrevier''',
                [lut_code]
            )
            result = self.cursor.fetchone()
            if result:
                geom = GEOSGeometry(result[0])
                frevier.geom = geom
                frevier.save()

    def import_jagdrevier(self):
        self.stdout.write("Importing Jagdrevier objects...")
        self.cursor.execute(
            '''SELECT lut_code, name FROM "common"."lut_jagdrevier" ORDER BY lut_code''')
        rtype, _ = RegionType.objects.get_or_create(name='Jagdrevier')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for row in self.cursor.fetchall():
            lut_code, name = row
            if lut_code < 1:
                continue
            jrevier, created = AdminRegion.objects.get_or_create(nr=lut_code, name=name, region_type=rtype)
            self.cursor.execute(
                '''SELECT ST_Multi(ST_Union(f.geom)) FROM admin.admin_einteilung_blbs AS f WHERE f.jagdrevier=%s GROUP BY f.jagdrevier''',
                [lut_code]
            )
            geom = GEOSGeometry(self.cursor.fetchone()[0])
            jrevier.geom = geom
            jrevier.save()

    def import_wep(self):
        self.stdout.write("Importing WEP objects...")
        self.cursor.execute(
            '''SELECT lut_code, name, array_agg(gemeinde_id_bfs) AS gemeinde_id_list
               FROM common.lut_wep_name AS wn
               LEFT JOIN common.lut_wep_perimeter AS wp ON (wn.lut_code=wp.wep_id) WHERE wn.lut_code>0
               GROUP BY lut_code, name ORDER BY wn.lut_code;''')
        rtype, _ = RegionType.objects.get_or_create(name='WEP')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for lut_code, name, gemeinden in self.cursor.fetchall():
            wep = AdminRegion.objects.create(nr='wep%s' % lut_code, name=name, region_type=rtype)
            self.cursor.execute(
                '''SELECT ST_Multi(ST_Union(f.geom)) FROM  admin.admin_einteilung_blbs AS f
                   WHERE f.gemeinde_id_bfs IN %s''',
                [tuple(gemeinden)]
            )
            geom = GEOSGeometry(self.cursor.fetchone()[0])
            wep.geom = geom
            wep.save()

    def import_eigentumer(self):
        self.stdout.write("Importing Eigentümer objects...")
        self.cursor.execute(
            '''SELECT min(id) AS nr, akt_name, ST_Multi(ST_Union(f.geom)) AS geom
               FROM fp.v_waldeigentum_detail f
               WHERE akt_name <> '' GROUP BY akt_name''',
            []
        )
        rtype, _ = RegionType.objects.get_or_create(name='Eigentümer')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for nr, eigen, geom in self.cursor.fetchall():
            AdminRegion.objects.create(
                nr='e%s' % nr, name=eigen, geom=GEOSGeometry(geom), region_type=rtype
            )
