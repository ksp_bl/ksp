# -*- coding: utf-8 -*-

from fabric.api import *
from fabric.utils import abort

env.hosts = ['guaraci.vserver.softronics.ch:2222']

project = 'ksp'
default_db_owner = 'claude'

def deploy():
    python_exec = "/var/www/virtualenvs/%s3/bin/python" % project

    with cd("/var/www/%s" % project):
        # activate maintenance mode
        run('sed -i -e "s/UPGRADING = False/UPGRADING = True/" ksp/wsgi.py')
        run('git stash && git pull && git stash pop')
        run('%s manage.py migrate' % python_exec)
        run('%s manage.py collectstatic --noinput' % python_exec)
        run('sed -i -e "s/UPGRADING = True/UPGRADING = False/" ksp/wsgi.py')

def clone_remote_pgdb(dbname):
    """ Dump a remote database and load it locally """
    def exist_local_db(db):
        db_list = local('sudo -u postgres psql --list', capture=True)
        return db in db_list
    def exist_username(db):
        user_list = local('sudo -u postgres psql -c "select usename from pg_user;"', capture=True)
        return db in user_list
    run('touch %(db)s.sql && chmod o+rw %(db)s.sql' % {'db': dbname})
    sudo('pg_dump --no-owner --no-privileges %(db)s > %(db)s.sql' % {'db': dbname}, user='postgres')
    get('%(db)s.sql' % {'db': dbname}, '.')

    if exist_local_db(dbname):
        rep = prompt('A local database named "%s" already exists. Overwrite? (y/n)' % dbname)
        if rep == 'y':
            local('''sudo -u postgres psql -c "DROP DATABASE %(db)s;"'''  % {'db': dbname})
        else:
            abort("Database not copied")

    if exist_username(dbname):
        owner = dbname
    else:
        owner = default_db_owner
    local('''sudo -u postgres psql -c "CREATE DATABASE %(db)s OWNER=%(owner)s;"''' % {
        'db': dbname, 'owner': owner})
    local('''sudo -u postgres psql -c "CREATE EXTENSION IF NOT EXISTS postgis;"''')
    local('''psql -U %(owner)s -W %(db)s < %(db)s.sql''' % {'db': dbname, 'owner': owner})
