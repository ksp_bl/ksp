"""
Find plots outside of its municipality area, and at which distance.
"""
from __future__ import unicode_literals
from django.db.models import F
from gemeinde.models import Gemeinde
from observation.models import Plot, PlotObs, UpdatedValue

plots = set([(obs, obs.plot, obs.municipality)
             for obs in PlotObs.objects.filter(plot__the_geom__disjoint=F('municipality__the_geom'))])

for obs, plot, muni in plots:
    gem = Gemeinde.objects.filter(the_geom__contains=plot.the_geom)
    #print("Plot %(plot)s: %(dist).2fm. from %(muni)s %(compl)s" % {
    #    'plot':plot, 'muni': muni, 'dist': plot.the_geom.distance(muni.the_geom),
    #    'compl': '(should be in %s)' % gem[0] if gem else '',
    #})
    #print("%s\t%s\t%s\t%s\t%s" % (
    #    plot.nr, plot.get_absolute_url(), plot.the_geom.distance(muni.the_geom), muni, gem[0] if gem else ''))
    #continue
    print(
        "Plot URL: %s\nPlot obs: %s\nCurrent municip.: %s\nProposed municip.: %s\nDistance: %.2fm.\n" % (
            'http://ksp.guaraci.ch%s' % plot.get_absolute_url(),
            ", ".join('%s-%s' % (o.year, o.municipality) for o in plot.plotobs_set.all()),
            obs.municipality,
            gem[0] if gem else '-',
            plot.the_geom.distance(muni.the_geom),
        )
    )
    resp = raw_input('Do you want to make the change ? (y/n) ')
    if resp == 'y':
        UpdatedValue.objects.create(table_name=PlotObs._meta.db_table, row_id=obs.pk,
            field_name='municipality_id', old_value=obs.municipality_id, new_value=gem[0].pk,
            comment="Script modification on March 27. 2015")
        obs.municipality = gem[0]
        obs.save()
    elif resp == 'q':
        break

