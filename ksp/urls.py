from django.conf import settings
from django.conf.urls import include, static, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

from observation import views
from imports import views as import_views


urlpatterns = [
    url(r'^$', views.GemeindenView.as_view(), name='home'),
    url(r'^gemeinde/(?P<pk>\d+)/$', views.GemeindeDetailView.as_view(), name='gemeinde'),
    # Views returning GeoJSON data
    url(r'^plotobs/$', views.PlotObsDataView.as_view(), name='plotobs'),
    url(r'^plotobs/(?P<pk>\d+)/json/$', views.PlotObsDetailView.as_view(output='json'), name='plotobs_detail_json'),
    url(r'^plotobs/(?P<pk>\d+)/$', views.PlotObsDetailView.as_view(output='html'), name='plotobs_detail'),
    url(r'^plot/(?P<pk>\d+)/$', views.PlotDetailView.as_view(), name='plot_detail'),
    # Temporary view:
    url(r'^plot/tobechecked/$', views.plots_to_check),
    url(r'^tree/reconcile/$', views.TreeReconcileView.as_view(), name='tree_reconcile'),
    url(r'^tree/(?P<pk>\d+)/edit/$', views.TreeEditView.as_view(), name='tree_edit'),
    url(r'^data/$', views.DataPageView.as_view(), name='data_page'),
    url(r'^data/grid/$', views.DataGridView.as_view(), name='data_grid'),
    url(r'^docs/$', views.DocumentationView.as_view(), name='docs'),
    url(r'^view_def/(?P<oid>\d+)/$', views.ViewDefinition.as_view(), name='view_def'),

    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),

    url(r'chart/(?P<view_name>\w+)/$', views.chart, name="view_chart"),

    url(r'^import/form/$', import_views.ImportView.as_view(), name="import"),
    url(r'^import/$', import_views.ImportedListView.as_view(), name="import_list"),

    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
    # Serve media files in development
    urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
