import re

from django.conf import settings
from django.http import HttpResponseRedirect

EXEMPT_URLS = [re.compile(settings.LOGIN_URL.lstrip('/'))]

class LoginRequiredMiddleware(object):
    def process_request(self, request):
        if not request.user.is_authenticated():
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                return HttpResponseRedirect("%s?next=%s" % (settings.LOGIN_URL, request.path_info))

