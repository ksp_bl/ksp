class DefaultRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'afw':
            return 'afw'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to auth_db.
        """
        if model._meta.app_label == 'afw':
            raise Exception("The afw database is read only")
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth app only appears in the 'auth_db'
        database.
        """
        if app_label == 'afw':
            return False
        return None

